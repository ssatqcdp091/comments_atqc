package com.softserve.edu.comments.tools;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Danil Zhyliaiev
 */
public class SelectWrapper extends ControlWrapper {
    protected Select select;

    public SelectWrapper(WebElement webElement) {
        super(webElement);
        this.select = new Select(webElement);
    }

    public Select getSelect() {
        return select;
    }

    public void deselectAll() {
        getSelect().deselectAll();
    }

    public void deselectByIndex(int index) {
        getSelect().deselectByIndex(index);
    }

    public void deselectByValue(String value) {
        getSelect().deselectByValue(value);
    }

    public void deselectByVisibleText(String text) {
        getSelect().deselectByVisibleText(text);
    }

    public List<String> getAllSelectedOptions() {
        List<String> selectedOptions = new ArrayList<>();
        for (WebElement option : getSelect().getAllSelectedOptions()) {
            selectedOptions.add(option.getText());
        }
        return selectedOptions;
    }

    public WebElement getFirstSelectedOption() {
        return getSelect().getFirstSelectedOption();
    }

    public String getSelectedOptionIndex() {
        return getSelect().getFirstSelectedOption().getAttribute("index");
    }

    public String getSelectedOptionValue() {
        return getSelect().getFirstSelectedOption().getAttribute("value");
    }

    public String getSelectedOptionText() {
        return getSelect().getFirstSelectedOption().getText();
    }

    public List<String> getOptions() {
        List<String> options = new ArrayList<>();
        for (WebElement option : getSelect().getOptions()) {
            options.add(option.getText());
        }
        return options;
    }

    public boolean isMultiple() {
        return getSelect().isMultiple();
    }

    public void selectByIndex(int index) {
        getSelect().selectByIndex(index);
    }

    public void selectByValue(String value) {
        getSelect().selectByValue(value);
    }

    public void selectByVisibleText(String text) {
        getSelect().selectByVisibleText(text);
    }
}
