package com.softserve.edu.comments.tools.specification;

import com.softserve.edu.comments.models.Comment;
import com.softserve.edu.comments.tools.controls.contracts.*;
import com.softserve.edu.comments.tools.controls.criteria.*;
import org.testng.Assert;

import java.util.List;

/**
 * @author Danil Zhyliaiev
 */
public class Specification {
    private boolean summaryResult;
    private StringBuilder summaryDescription;

    private Specification() {
        this.summaryResult = true;
        this.summaryDescription = new StringBuilder();
    }

    public static Specification get() {
        return new Specification();
    }

    public boolean getPassed() {
        return summaryResult;
    }

    public String getDescription() {
        return summaryDescription.toString();
    }

    public void add(boolean pass, String errorText) {
        summaryResult = summaryResult && pass;

        if (!pass) {
            summaryDescription.append(errorText);
        }
    }

    public void check() {
        Assert.assertTrue(summaryResult, summaryDescription.toString());
    }

    public CheckboxCriteria For(Checkbox checkbox) {
        return CheckboxCriteria.get(checkbox, this);
    }

    public DropdownCriteria For(Dropdown dropdown) {
        return DropdownCriteria.get(dropdown, this);
    }

    public LabelCriteria For(Label label) {
        return LabelCriteria.get(label, this);
    }

    public LinkCriteria For(Link link) {
        return LinkCriteria.get(link, this);
    }

    public TextInputCriteria For(TextInput textInput) {
        return TextInputCriteria.get(textInput, this);
    }

    public ButtonCriteria For(Button button) {
        return ButtonCriteria.get(button, this);
    }

    public LableListCriteria For(List<Label> lables) {
        return LableListCriteria.get(lables, this);
    }

    public DigitCriteria For(Integer digit) {
        return DigitCriteria.get(digit, this);
    }

    public CommentCriteria For(Comment comment) {
        return CommentCriteria.get(comment, this);
    }
}
