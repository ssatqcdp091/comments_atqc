package com.softserve.edu.comments.tools.specification;

/**
 * @author Danil Zhyliaiev
 */
public interface Specifiable {
    Specification next();
}
