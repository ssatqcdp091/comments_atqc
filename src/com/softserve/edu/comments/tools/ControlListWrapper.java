package com.softserve.edu.comments.tools;

import com.softserve.edu.comments.tools.ControlWrapper;
import com.softserve.edu.comments.tools.SelectWrapper;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

/**
 * Class to wrap a list of WebElement's with list of ControlWrapper's
 * or optionally list of SelectWrapper's.
 * @author Danil Zhyliaiev
 */
public class ControlListWrapper {
    private List<ControlWrapper> controls;
    private List<WebElement> webElements;
    private List<SelectWrapper> selects;

    public ControlListWrapper(List<WebElement> webElements) {
        controls = new ArrayList<>();
        this.webElements = webElements;

        for (WebElement webElement : webElements) {
            controls.add(new ControlWrapper(webElement));
        }
    }

    public List<ControlWrapper> get() {
        return controls;
    }

    public List<SelectWrapper> getSelects() {
        if (selects == null) {
            selects = new ArrayList<>();

            for (WebElement webElement : webElements) {
                selects.add(new SelectWrapper(webElement));
            }
        }

        return selects;
    }
}
