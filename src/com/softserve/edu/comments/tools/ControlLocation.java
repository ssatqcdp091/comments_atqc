package com.softserve.edu.comments.tools;

import org.openqa.selenium.By;


/**
 * @author J.Bodnar on 11.02.2016.
 */
public class ControlLocation {

    private By by;
    private String value;

    private ControlLocation(By by, String value) {
        this.value = value;
        this.by = by;
    }

    public static ControlLocation getById(String id) {
        return new ControlLocation(By.id(id) , id);
    }

    public static ControlLocation getByName(String searchName) {
        return new ControlLocation(By.name(searchName), searchName);
    }

    public static ControlLocation getByXPath(String xpathExpression)  {
        return new ControlLocation(By.xpath(xpathExpression), xpathExpression);
    }

    public static ControlLocation getByCss(String cssSelector)  {
        return new ControlLocation(By.cssSelector(cssSelector), cssSelector);
    }

    public static ControlLocation getByLinkText (String linkText)  {
        return new ControlLocation(By.linkText(linkText), linkText);
    }

    public String getValue() {
        return value;
    }

    public By getBy() {
        return by;
    }
}
