package com.softserve.edu.comments.tools;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public class WebDriverUtils {
    public static final int ABSENCE_TIMEOUT = 5;
    private static WebDriver driver;
    private static long implicitlyWaitTimeout = 20;

    private WebDriverUtils() {
    }

    public static WebDriver getDriver() {
  //      System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
//        System.setProperty("webdriver.firefox.driver", "C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe");
        if (driver == null) {
       //   driver = new ChromeDriver();
           driver = new FirefoxDriver();
            driver.manage().timeouts().implicitlyWait(
                    getImplicitlyWaitTimeout(), TimeUnit.SECONDS);
            driver.manage().window().maximize();
        }
        return driver;
    }

    public static long getImplicitlyWaitTimeout() {
        return implicitlyWaitTimeout;
    }

    public static void load(String path) {
        driver.navigate().to(path);
    }

    public static void stop() {
        if (driver != null) {
            driver.close();
        }
        driver = null;
    }

    public static void refresh() {
        getDriver().navigate().refresh();
    }

    public static String getCurrentUrl() {
        return driver.getCurrentUrl();
    }

    public static String getTitle(){
        return driver.getTitle();
    }
}
