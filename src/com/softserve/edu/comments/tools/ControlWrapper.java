package com.softserve.edu.comments.tools;

import org.openqa.selenium.WebElement;

public class ControlWrapper {
    private WebElement searchContext;

    public ControlWrapper(WebElement searchContext) {
        this.searchContext = searchContext;
    }

    public WebElement get() {
        return searchContext;
    }

    public void click() {
        get().click();
    }

    public void clear() {
        get().clear();
    }

    public void submit() {
        get().submit();
    }

    public boolean isSelected() {
        return get().isSelected();
    }

    public boolean isEnabled() {
        return get().isEnabled();
    }

    public boolean isDisplayed() {
        return get().isDisplayed();
    }

    public String getText() {
        return get().getText();
    }

    public String getValue() {
        return get().getAttribute("value");
    }

    public void sendKeys(String text) {
        get().sendKeys(text);
    }

    public String getAttribute(String s) {
        return get().getAttribute(s);
    }
}
