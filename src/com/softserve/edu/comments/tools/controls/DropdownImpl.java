package com.softserve.edu.comments.tools.controls;

import com.softserve.edu.comments.tools.ContextVisible;
import com.softserve.edu.comments.tools.ControlLocation;
import com.softserve.edu.comments.tools.SelectWrapper;
import com.softserve.edu.comments.tools.controls.contracts.Dropdown;

import java.util.List;

/**
 * @author Danil Zhyliaiev
 */
public class DropdownImpl implements Dropdown {
    private SelectWrapper select;

    protected DropdownImpl(SelectWrapper select) {
        this.select = select;
    }

    public static Dropdown getById(String id) {
        return get(ControlLocation.getById(id));
    }

    public static Dropdown getByName(String searchName) {
        return get(ControlLocation.getByName(searchName));
    }

    public static Dropdown getByCss(String cssSelector) {
        return get(ControlLocation.getByCss(cssSelector));
    }

    public static Dropdown getByXPath(String xpathExpression) {
        return get(ControlLocation.getByXPath(xpathExpression));
    }

    private static Dropdown get(ControlLocation controlLocation) {
        return new DropdownImpl(new SelectWrapper(
                ContextVisible.get().get(controlLocation)));
    }

    @Override
    public String getSelectedOptionIndex() {
        return select.getSelectedOptionIndex();
    }

    @Override
    public String getSelectedOptionValue() {
        return select.getSelectedOptionValue();
    }

    @Override
    public String getSelectedOptionText() {
        return select.getSelectedOptionText();
    }

    @Override
    public List<String> getOptions() {
        return select.getOptions();
    }

    @Override
    public boolean isEnabled() {
        return select.isEnabled();
    }

    @Override
    public void selectByIndex(final int index) {
        select.selectByIndex(index);
    }

    @Override
    public void selectByValue(final String value) {
        select.selectByValue(value);
    }

    @Override
    public void selectByVisibleText(final String text) {
        select.selectByVisibleText(text);
    }
}
