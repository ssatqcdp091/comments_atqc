package com.softserve.edu.comments.tools.controls;

import com.softserve.edu.comments.tools.ContextVisible;
import com.softserve.edu.comments.tools.ControlLocation;
import com.softserve.edu.comments.tools.ControlWrapper;
import com.softserve.edu.comments.tools.controls.contracts.Button;

public class ButtonImpl implements Button {


    private ControlWrapper control;

    protected ButtonImpl(ControlWrapper control) {
        this.control = control;
    }


    public static Button getByXpath(String xpathExpression) {
        return get(ControlLocation.getByXPath(xpathExpression));
    }

    public static Button getById(String id) {
        return get(ControlLocation.getById(id));
    }

    public static Button getByName(String name) {
        return get(ControlLocation.getByName(name));
    }

    public static Button getByCss(String cssSelector) {
        return get(ControlLocation.getByCss(cssSelector));
    }

    public static Button getByLinkText(String linkText) {
        return get(ControlLocation.getByLinkText(linkText));
    }

    private static Button get(ControlLocation controlLocation) {
        return new ButtonImpl(new ControlWrapper(ContextVisible.get().get(controlLocation))) {
        };

    }

    public boolean isDisplayed() {
        return control.isDisplayed();
    }

    public boolean isEnabled() {
        return control.isEnabled();
    }

    public String getText() {
        return control.getValue();
    }

    public void click() {
        control.click();
    }
}
