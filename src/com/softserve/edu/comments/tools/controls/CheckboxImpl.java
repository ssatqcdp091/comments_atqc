package com.softserve.edu.comments.tools.controls;

import com.softserve.edu.comments.tools.ContextVisible;
import com.softserve.edu.comments.tools.ControlLocation;
import com.softserve.edu.comments.tools.ControlWrapper;
import com.softserve.edu.comments.tools.controls.contracts.Checkbox;

public class CheckboxImpl implements Checkbox {

    private ControlWrapper control;

    protected CheckboxImpl(ControlWrapper control) {
        this.control = control;
    }

    public static Checkbox get(ControlLocation controlLocation) {
        return new CheckboxImpl(new ControlWrapper(ContextVisible.get().get(controlLocation)));
    }

    public static Checkbox getByCss(String cssSelector) {
        return get(ControlLocation.getByCss(cssSelector));
    }

    public static Checkbox getByXpath(String xpathExpression) {
        return get(ControlLocation.getByXPath(xpathExpression));
    }

    public static Checkbox getById(String id) {
        return get(ControlLocation.getById(id));
    }

    @Override
    public void checkOff() {
        if (!control.get().isSelected()) {
            control.click();
        }
    }

    @Override
    public void unCheck() {
        if (control.get().isSelected()) {
            control.click();
        }
    }

    @Override
    public boolean isCheckedOff() {
        return control.isSelected();
    }

    @Override
    public boolean isEnabled() {
        return control.isEnabled();
    }
}
