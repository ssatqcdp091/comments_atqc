package com.softserve.edu.comments.tools.controls;

import com.softserve.edu.comments.tools.ContextVisible;
import com.softserve.edu.comments.tools.ControlLocation;
import com.softserve.edu.comments.tools.ControlWrapper;
import com.softserve.edu.comments.tools.controls.contracts.CustomElement;

/**
 * Class to represent custom div and elements.
 * @author Danil Zhyliaiev
 */
public class CustomElementImpl implements CustomElement {
    private ControlWrapper control;

    protected CustomElementImpl(ControlWrapper control) {
        this.control = control;
    }

    public static CustomElement getById(String id) {
        return get(ControlLocation.getById(id));
    }

    public static CustomElement getByName(String searchedName) {
        return get(ControlLocation.getByName(searchedName));
    }

    public static CustomElement getByLinkText(String linkText) {
        return get(ControlLocation.getByLinkText(linkText));
    }

    public static CustomElement getByXpath(String xpathExpression) {
        return get(ControlLocation.getByXPath(xpathExpression));
    }

    public static CustomElement getByCss(String cssSelector) {
        return get(ControlLocation.getByCss(cssSelector));
    }

    private static CustomElement get(ControlLocation controlLocation) {
        return new CustomElementImpl(new ControlWrapper(ContextVisible.get().get(controlLocation)));
    }

    @Override
    public void click() {
        control.click();
        }

    @Override
    public void clear() {
        control.clear();
    }

    @Override
    public void submit() {
        control.submit();
    }

    @Override
    public boolean isSelected() {
        return control.isSelected();
    }

    @Override
    public boolean isEnabled() {
        return control.isEnabled();
    }

    @Override
    public boolean isDisplayed() {
        return control.isDisplayed();
    }

    @Override
    public String getText() {
        return control.getText();
    }

    @Override
    public void sendKeys(final String text) {
        control.sendKeys(text);
    }
}
