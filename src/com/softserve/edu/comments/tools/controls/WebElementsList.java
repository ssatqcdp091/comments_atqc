package com.softserve.edu.comments.tools.controls;

import com.softserve.edu.comments.tools.*;
import com.softserve.edu.comments.tools.controls.contracts.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Class used to handle and represent list of ControlWrapper's as list of
 * needed Controls.
 * @author Danil Zhyliaiev
 */
public class WebElementsList {
    private List<ControlWrapper> controls;
    private ControlListWrapper controlListWrapper;

    private WebElementsList(ControlListWrapper controlListWrapper) {
        this.controlListWrapper = controlListWrapper;
        this.controls = controlListWrapper.get();

    }

    public List<Button> asButtons() {
        List<Button> checkboxes = new ArrayList<>();

        for (ControlWrapper control : controls) {
            checkboxes.add(new ButtonImpl(control));
        }

        return checkboxes;
    }
    
    public List<Checkbox> asCheckboxes() {
        List<Checkbox> checkboxes = new ArrayList<>();

        for (ControlWrapper control : controls) {
            checkboxes.add(new CheckboxImpl(control));
        }

        return checkboxes;
    }

    public List<Dropdown> asDropdowns() {
        List<Dropdown> dropdowns = new ArrayList<>();

        for (SelectWrapper control : controlListWrapper.getSelects()) {
            dropdowns.add(new DropdownImpl(control));
        }

        return dropdowns;
    }

    public List<Label> asLabels() {
        List<Label> labels = new ArrayList<>();

        for (ControlWrapper control : controls) {
            labels.add(new LabelImpl(control));
        }

        return labels;
    }

    public List<Link> asLinks() {
        List<Link> links = new ArrayList<>();

        for (ControlWrapper control : controls) {
            links.add(new LinkImpl(control));
        }

        return links;
    }

    public List<TextInput> asTextInputs() {
        List<TextInput> textInputs = new ArrayList<>();

        for (ControlWrapper control : controls) {
            textInputs.add(new TextInputImpl(control));
        }

        return textInputs;
    }

    public List<CustomElement> asCustomElements() {
        List<CustomElement> textInputs = new ArrayList<>();

        for (ControlWrapper control : controls) {
            textInputs.add(new CustomElementImpl(control));
        }

        return textInputs;
    }


    public static WebElementsList getById(String id) {
        return get(ControlLocation.getById(id));
    }

    public static WebElementsList getByName(String searchName) {
        return get(ControlLocation.getByName(searchName));
    }

    public static WebElementsList getByCss(String cssSelector) {
        return get(ControlLocation.getByCss(cssSelector));
    }

    public static WebElementsList getByXPath(String xpathExpression) {
        return get(ControlLocation.getByXPath(xpathExpression));
    }

    private static WebElementsList get(ControlLocation controlLocation) {
        return new WebElementsList(new ControlListWrapper(
                ContextVisible.get().getElements(controlLocation)));
    }
}
