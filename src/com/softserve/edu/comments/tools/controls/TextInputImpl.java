package com.softserve.edu.comments.tools.controls;


import com.softserve.edu.comments.tools.ContextVisible;
import com.softserve.edu.comments.tools.ControlLocation;
import com.softserve.edu.comments.tools.ControlWrapper;
import com.softserve.edu.comments.tools.controls.contracts.TextInput;

/**
 * @author J.Bodnar on 11.02.2016.
 */
public class TextInputImpl implements TextInput {
    private ControlWrapper control;

    protected TextInputImpl(ControlWrapper control) {
        this.control = control;
    }

    public static TextInput getByXpath(String xpathExpression){
        return get(ControlLocation.getByXPath(xpathExpression));
    }

    public static TextInput getById(String id){
        return get(ControlLocation.getById(id));
    }

    public static TextInput getByName(String name){
        return get(ControlLocation.getByName(name));
    }
    public static TextInput getByCss(String cssSelector){
        return get(ControlLocation.getByCss(cssSelector));
    }
    public static TextInput getByLinkText(String linkText){
        return get(ControlLocation.getByLinkText(linkText));
    }

    private static TextInput get(ControlLocation controlLocation) {
        return new TextInputImpl(new ControlWrapper(ContextVisible.get().get(controlLocation)));
        }

    @Override
    public boolean isDisplayed() {
        return control.isDisplayed();
    }

    @Override
    public void clear() {
        control.clear();
    }

    @Override//??
    public void type(String text) {
        control.sendKeys(text);
    }

    @Override
    public void submit() {
        control.submit();
    }

    @Override
    public void click() {
        control.click();
    }

    @Override
    public String getText() {
        return control.getValue();
    }
}

