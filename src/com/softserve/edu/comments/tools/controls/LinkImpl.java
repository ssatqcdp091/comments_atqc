
/**
 * Author S.Tsiganovskiy
 */
package com.softserve.edu.comments.tools.controls;

import com.softserve.edu.comments.tools.ContextVisible;
import com.softserve.edu.comments.tools.ControlLocation;
import com.softserve.edu.comments.tools.ControlWrapper;
import com.softserve.edu.comments.tools.controls.contracts.Link;


public class LinkImpl implements Link {
    private ControlWrapper control;

    protected LinkImpl(ControlWrapper control) {
        this.control = control;
    }

    public static Link getById(String id) {
        return get(ControlLocation.getById(id));
    }

    public static Link getByName(String searchedName) {
        return get(ControlLocation.getByName(searchedName));
    }

    public static Link getByLinkText(String linkText) {
        return get(ControlLocation.getByLinkText(linkText));
    }

    public static Link getByXpath(String xpathExpression) {
        return get(ControlLocation.getByXPath(xpathExpression));
    }

    public static Link getByCss(String cssSelector) {
        return get(ControlLocation.getByCss(cssSelector));
    }

    public static Link get(ControlLocation controlLocation) {
        return new LinkImpl(new ControlWrapper(ContextVisible.get().get(controlLocation)));
    }

    public String getHref() {
        return control.getAttribute("href");
    }

    public void click() {
        control.click();
    }

    public String getText() {
        return control.getText();
    }
}