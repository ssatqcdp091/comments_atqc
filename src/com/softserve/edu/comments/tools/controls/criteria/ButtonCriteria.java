package com.softserve.edu.comments.tools.controls.criteria;

import com.softserve.edu.comments.tools.controls.contracts.Button;
import com.softserve.edu.comments.tools.specification.Specifiable;
import com.softserve.edu.comments.tools.specification.Specification;
import org.openqa.selenium.NotFoundException;

/**
 * @author Danil Zhyliaiev
 */
public class ButtonCriteria implements Specifiable{
    private Button button;
    private Specification specification;

    private ButtonCriteria(Button button, Specification specification) {
        if (button == null) {
            throw new NotFoundException("the income Button == null");
        }
        this.button = button;
        this.specification = specification;
    }

    public static ButtonCriteria get(Button button, Specification specification) {
        return new ButtonCriteria(button, specification);
    }

    public ButtonCriteria isVisible() {
        this.specification.add(this.button.isDisplayed(), "Button isn't visible.");
        return this;
    }

    public ButtonCriteria isEnabled() {
        this.specification.add(this.button.isEnabled(), "Button isn't enabled.");
        return this;
    }

    public ButtonCriteria isDisabled() {
        this.specification.add(!this.button.isEnabled(), "Button isn't disabled.");
        return this;
    }

    public ButtonCriteria textMatch(String expectedText) {
        this.specification.add(expectedText.equals(this.button.getText()), "Text doesn't match.");
        return this;
    }

    @Override
    public Specification next() {
        return this.specification;
    }
}
