package com.softserve.edu.comments.tools.controls.criteria;

import com.softserve.edu.comments.tools.controls.contracts.TextInput;
import com.softserve.edu.comments.tools.specification.Specifiable;
import com.softserve.edu.comments.tools.specification.Specification;
import org.openqa.selenium.NotFoundException;

/**
* @ J.Bodnar on 12.02.2016.
        */
public class TextInputCriteria implements Specifiable {

    private TextInput textInput;
    private Specification specification;

    private TextInputCriteria(TextInput textInput, Specification specification){
        if (textInput == null) {
            throw new NotFoundException("the income TextInput == null");
        }

        this.textInput = textInput;
        this.specification = specification;
    }

    public static TextInputCriteria get(TextInput textInput, Specification specification){
        return new TextInputCriteria(textInput, specification);
    }

    public TextInputCriteria isVisible(){
        this.specification.add(this.textInput.isDisplayed(), "It's not visible.");
        return this;
    }
    public TextInputCriteria isEmpty(){
        this.specification.add("".equals(this.textInput.getText()), "Input is not empty.");
        return this;
    }
    public TextInputCriteria textMatch(String expectedResult){
        this.specification.add(expectedResult.equals(this.textInput.getText()), "Text does not match.");
        return this;
    }

    @Override
    public Specification next() {
        return this.specification;
    }

}
