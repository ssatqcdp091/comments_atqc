package com.softserve.edu.comments.tools.controls.criteria;


import com.softserve.edu.comments.tools.controls.contracts.Label;
import com.softserve.edu.comments.tools.specification.Specifiable;
import com.softserve.edu.comments.tools.specification.Specification;
import org.openqa.selenium.NotFoundException;

/**Author S.Tsiganovskiy*/

public class LabelCriteria implements Specifiable {
    private Label label;
    private Specification specification;

    private LabelCriteria(Label label, Specification specification){
        if (label == null) {
            throw new NotFoundException("the income Label == null");
        }

        this.label = label;
        this.specification = specification;
    }

    public static LabelCriteria get(Label label, Specification specification){
        return new LabelCriteria(label, specification);
    }
    // verify if the text of the label is as expected
    public LabelCriteria textMatch(String expectedResult){
        this.specification.add(this.label.getText().equals(expectedResult),"Text doesn't match.");
        return this;
    }

    //verify if the label is visible
    public LabelCriteria isVisible(){
        this.specification.add(this.label.isDisplayed(), "It's not visible.");
        return this;
    }

    public Specification next(){
        return this.specification;
    }
}


