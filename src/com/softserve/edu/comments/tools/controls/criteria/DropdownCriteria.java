package com.softserve.edu.comments.tools.controls.criteria;

import com.softserve.edu.comments.tools.controls.contracts.Dropdown;
import com.softserve.edu.comments.tools.specification.Specifiable;
import com.softserve.edu.comments.tools.specification.Specification;
import org.openqa.selenium.NotFoundException;

import java.util.List;

/**Author S.Tsiganovskiy*/
public class DropdownCriteria implements Specifiable {
    private Dropdown dropdown;
    private Specification specification;

    private DropdownCriteria(Dropdown dropdown, Specification specification){
        if (dropdown == null) {
            throw new NotFoundException("the income Dropdown == null");
        }

        this.dropdown = dropdown;
        this.specification = specification;
    }

    public static DropdownCriteria get(Dropdown dropdown, Specification specification) {
        return new DropdownCriteria(dropdown, specification);
    }

    // Compare visible text ( or an option)  with expected string
    public DropdownCriteria visibleTextMatch(String expectedResult){
        this.specification.add(this.dropdown.getSelectedOptionText().equals(expectedResult),"Text doesn't match.");
        return this;
    }

    // Compare value of an option  with expected string
    public DropdownCriteria valueMatch(String expectedResult){
        this.specification.add(this.dropdown.getSelectedOptionValue().equals(expectedResult),"Value doesn't match.");
        return this;
    }

    /**
     * Compare current optionsList with expected one
     *
     * @param expectedOptionsList List<String> of options the Dropdown should contain according to spec
     * @return DropdownCriteria
     */
    public DropdownCriteria optionsListMatch(List<String> expectedOptionsList) {
        specification.add(dropdown.getOptions().equals(expectedOptionsList), "OptionsList doesn't match.");
        return this;
    }

    /**
     * check if the dropdown is enabled
     *
     * @return DropdownCriteria
     */
    public DropdownCriteria isEnabled() {
        specification.add(dropdown.isEnabled(), "The Dropdown is non enabled.");
        return this;
    }

     public Specification next(){
        return this.specification;
    }
}
