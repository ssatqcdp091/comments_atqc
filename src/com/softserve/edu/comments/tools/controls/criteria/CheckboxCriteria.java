package com.softserve.edu.comments.tools.controls.criteria;

import com.softserve.edu.comments.tools.controls.contracts.Checkbox;
import com.softserve.edu.comments.tools.specification.Specifiable;
import com.softserve.edu.comments.tools.specification.Specification;
import org.openqa.selenium.NotFoundException;

/**Author S.Tsiganovskiy*/

public class CheckboxCriteria implements Specifiable {
    private Checkbox checkbox;
    private Specification specification;

    private CheckboxCriteria(Checkbox checkbox, Specification specification){
        if (checkbox == null) {
            throw new NotFoundException("the income Checkbox == null");
        }
        this.checkbox = checkbox;
        this.specification = specification;
    }

    public static CheckboxCriteria get(Checkbox checkbox, Specification specification){
        return new CheckboxCriteria(checkbox, specification);
    }

    //Verify if the checkbox is checked
    public CheckboxCriteria isChecked(){
        this.specification.add(this.checkbox.isCheckedOff(),"The checkbox is not checked.");
        return this;
    }

    //Verify if the checkbox is unchecked
    public CheckboxCriteria isUnChecked(){
        this.specification.add(!this.checkbox.isCheckedOff(),"The checkbox is checked.");
        return this;
    }

    //Verify if the checkbox is enabled
    public CheckboxCriteria isEnabled(){
        this.specification.add(this.checkbox.isEnabled(),"The checkbox is disable.");
        return this;
    }

    public CheckboxCriteria statusMatch(boolean status) {
        this.specification.add(this.checkbox.isCheckedOff() == status, "Invalid checkbox status.");
        return this;
    }

    public Specification next(){
        return this.specification;
    }
}
