package com.softserve.edu.comments.tools.controls.criteria;

import com.softserve.edu.comments.tools.specification.Specification;
import org.openqa.selenium.NotFoundException;

public class DigitCriteria {
    private int digit;
    private Specification specification;

    private DigitCriteria(Integer digit, Specification specification) {
        if (digit == null) {
            throw new NotFoundException("the income Integer == null");
        }

        this.digit = digit;
        this.specification = specification;
    }

    public static DigitCriteria get(int numberOfCommentsOnPage, Specification specification) {
        return new DigitCriteria(numberOfCommentsOnPage, specification);
    }

    /**
     * check if the income digit not greater than max
     */
    public DigitCriteria notGreaterThan(Integer max) {
        this.specification.add(digit <= max, "The digit is greater than specified one.");
        return this;
    }

    public DigitCriteria valueMatch(Integer integer) {
        this.specification.add(integer.equals(this.digit), "The digit is not equals the specified one.");
        return this;
    }

    public Specification next() {
        return this.specification;
    }
}
