package com.softserve.edu.comments.tools.controls.criteria;

import com.softserve.edu.comments.tools.controls.contracts.Link;
import com.softserve.edu.comments.tools.specification.Specifiable;
import com.softserve.edu.comments.tools.specification.Specification;
import org.openqa.selenium.NotFoundException;

/**Author S.Tsiganovskiy*/
public class LinkCriteria implements Specifiable {
    private Link link;
    private Specification specification;

    private LinkCriteria(Link link, Specification specification){
        if (link == null) {
            throw new NotFoundException("the income Link == null");
        }
        this.link = link;
        this.specification = specification;
    }

    public static LinkCriteria get(Link link, Specification specification){
        return new LinkCriteria(link, specification);
    }

    // verify if the text of the link is as expected
    public LinkCriteria textMatch(String expectedResult){
        this.specification.add(this.link.getText().equals(expectedResult),"Text doesn't match.");
        return this;
    }

    // verify if the href of the link is as expected
    public LinkCriteria hrefMatch(String expectedResult) {
        this.specification.add(this.link.getHref().equals(expectedResult), " href doesn't match.");
        return this;
    }

    public Specification next(){
        return this.specification;
    }

}
