package com.softserve.edu.comments.tools.controls.criteria;

import com.softserve.edu.comments.models.Comment;
import com.softserve.edu.comments.tools.specification.Specification;
import org.openqa.selenium.NotFoundException;

import java.util.List;

public class CommentCriteria {
    private Comment comment;
    private Specification specification;

    private CommentCriteria(Comment comment, Specification specification) {
        if (comment == null) {
            throw new NotFoundException("the income comment == null");
        }

        this.comment = comment;
        this.specification = specification;
    }

    public static CommentCriteria get(Comment comment, Specification specification) {
        return new CommentCriteria(comment, specification);
    }

    public CommentCriteria containedInList(List<Comment> commentList) {
        this.specification.add(commentList.contains(this.comment), "List doesn't contain specified comment.");
        return this;
    }

    public CommentCriteria notContainedInList(List<Comment> commentList) {
        this.specification.add(!commentList.contains(this.comment), "List contains specified comment.");
        return this;
    }

    public Specification next() {
        return this.specification;
    }
}
