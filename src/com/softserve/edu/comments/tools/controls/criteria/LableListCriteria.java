package com.softserve.edu.comments.tools.controls.criteria;

import com.softserve.edu.comments.tools.controls.contracts.Label;
import com.softserve.edu.comments.tools.specification.Specification;
import org.openqa.selenium.NotFoundException;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Danil Zhyliaiev
 */
public class LableListCriteria {
    private List<String> list;
    private Specification specification;

    private LableListCriteria(List<Label> list, Specification specification) {
        if (list == null) {
            throw new NotFoundException("the income List<Label> == null");
        }

        this.list = new ArrayList<>();
        for (Label label : list) {
            this.list.add(label.getText());
        }
        this.specification = specification;
    }

    public static LableListCriteria get(List<Label> list, Specification specification) {
        return new LableListCriteria(list, specification);
    }

    public LableListCriteria containsAll(List<String> expectedList) {
        this.specification.add(this.list.containsAll(expectedList), "List doesn't contain all elements.");
        return this;
    }

    public LableListCriteria listSizeEquals(int expectedSize) {
        this.specification.add(this.list.size() == expectedSize, "List has wrong size.");
        return this;
    }

    public Specification next() {
        return this.specification;
    }
}
