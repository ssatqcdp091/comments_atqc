package com.softserve.edu.comments.tools.controls;

import com.softserve.edu.comments.tools.ContextVisible;
import com.softserve.edu.comments.tools.ControlLocation;
import com.softserve.edu.comments.tools.ControlWrapper;
import com.softserve.edu.comments.tools.controls.contracts.Label;

/**
 * @author J.Bodnar on 11.02.2016.
 */
public class LabelImpl implements Label {

    private ControlWrapper control;

    protected LabelImpl(ControlWrapper control) {
        this.control = control;
    }

    public static Label getByXpath(String xpathExpression){
        return get(ControlLocation.getByXPath(xpathExpression));
    }

    public static Label getById(String id){
        return get(ControlLocation.getById(id));
    }

    public static Label getByName(String name){
        return get(ControlLocation.getByName(name));
    }

    public static Label getByCss(String cssSelector){
        return get(ControlLocation.getByCss(cssSelector));
    }

    public static Label getByLinkText(String linkText){
        return get(ControlLocation.getByLinkText(linkText));
    }

    private static Label get(ControlLocation controlLocation) {
        return new LabelImpl(new ControlWrapper(ContextVisible.get().get(controlLocation))) {
        };

    }

    public String getText() {
        return control.getText();
    }

    public boolean isDisplayed() {
        return control.isDisplayed();
    }
}
