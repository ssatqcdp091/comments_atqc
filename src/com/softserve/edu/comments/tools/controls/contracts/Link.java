package com.softserve.edu.comments.tools.controls.contracts;

public interface Link {
    void click();

    String getText();

    String getHref();
}
