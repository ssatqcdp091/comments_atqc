package com.softserve.edu.comments.tools.controls.contracts;

/**
 * Created by Acer on 13.02.2016.
 */
public interface Button {

   boolean isDisplayed();

    boolean isEnabled();

    String getText();

    void click();
}
