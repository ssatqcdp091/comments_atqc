package com.softserve.edu.comments.tools.controls.contracts;

/**
 * Class to represent custom web element as divs, spans etc.
 * @author Danil Zhyliaiev
 */
public interface CustomElement {
    void click();

    void clear();

    void submit();

    boolean isSelected();

    boolean isEnabled();

    boolean isDisplayed();

    String getText();

    void sendKeys(String text);
}
