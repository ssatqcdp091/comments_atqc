package com.softserve.edu.comments.tools.controls.contracts;

public interface Checkbox {
    void checkOff();

    void unCheck();

    boolean isCheckedOff();

    boolean isEnabled();
}
