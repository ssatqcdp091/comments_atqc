package com.softserve.edu.comments.tools.controls.contracts;

public interface TextInput extends Label {

    void click();

    void clear();
    void type(String text);
    void submit();
}
