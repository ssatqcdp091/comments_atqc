package com.softserve.edu.comments.tools.controls.contracts;

public interface Label {

    boolean isDisplayed();

    String getText();
}
