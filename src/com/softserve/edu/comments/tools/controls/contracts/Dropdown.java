package com.softserve.edu.comments.tools.controls.contracts;

/**
 * @author Danil Zhyliaiev
 */

import java.util.List;

public interface Dropdown {
    String getSelectedOptionIndex();

    String getSelectedOptionValue();

    String getSelectedOptionText();

    List<String> getOptions();

    boolean isEnabled();

    void selectByIndex(int index);

    void selectByValue(String value);

    void selectByVisibleText(String text);
}
