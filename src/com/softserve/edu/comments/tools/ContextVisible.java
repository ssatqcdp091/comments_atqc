package com.softserve.edu.comments.tools;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author Danil Zhyliaiev
 */
public class ContextVisible {
    private ContextVisible() { }

    public static ContextVisible get() {
        return new ContextVisible();
    }

    public WebElement get(ControlLocation controlLocation) {
        WebDriverWait wait = new WebDriverWait(
                WebDriverUtils.getDriver(),
                WebDriverUtils.getImplicitlyWaitTimeout());

        WebElement webElement = wait.until(
                ExpectedConditions.
                        visibilityOfElementLocated(controlLocation.getBy()));

        if (webElement == null) {
            throw new NoSuchElementException(
                    String.format("Error %s", controlLocation.getValue()));
        }

        return webElement;
    }

    /**
     * Used to get a list of visible web elements that match given ControlLocation.
     * @param controlLocation   some locator.
     * @return                  List of visible WebElements found by given locator.
     */
    public List<WebElement> getElements(ControlLocation controlLocation) {
        turnOffTimeouts();
        List<WebElement> webElements = WebDriverUtils.getDriver().findElements(controlLocation.getBy());
        turnOnTimeouts();

        if (webElements.size() != 0) {
            WebDriverWait wait = new WebDriverWait(
                    WebDriverUtils.getDriver(),
                    0);

            webElements = wait.until(
                    ExpectedConditions
                            .visibilityOfAllElementsLocatedBy(controlLocation.getBy()));

            if (webElements == null) {
                throw new NoSuchElementException(
                        String.format("Error %s", controlLocation.getValue()));
            }
        }

        return webElements;
    }

    private void turnOffTimeouts() {
        WebDriverUtils.getDriver().manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
    }

    private void turnOnTimeouts() {
        WebDriverUtils.getDriver().manage().timeouts().implicitlyWait(WebDriverUtils.getImplicitlyWaitTimeout(),
                TimeUnit.SECONDS);
    }
}
