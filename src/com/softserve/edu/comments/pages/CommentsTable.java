package com.softserve.edu.comments.pages;

import com.softserve.edu.comments.models.Comment;
import com.softserve.edu.comments.pages.uimaps.CommentsTableUIMap;
import com.softserve.edu.comments.pages.uimaps.CommentsTableUIMap.TableRow;
import com.softserve.edu.comments.tools.controls.contracts.Label;
import com.softserve.edu.comments.tools.controls.contracts.Link;

import java.util.ArrayList;
import java.util.List;

/**
 * @author J.Bodnar on 14.02.2016.
 */

public class CommentsTable {

    private CommentsTableUIMap controlls;

    public CommentsTable() {
        controlls = new CommentsTableUIMap();
    }

    //getters
    public Link getNumberSortLink() {
        return controlls.getNumberSortLink();
    }

    public Link getCommentTextSortLink() {
        return controlls.getCommentTextSortLink();
    }

    public Link getActiveStatusSortLink() {
        return controlls.getCommentTextSortLink();
    }

    public Label getCategoriesColumnLabel() {
        return controlls.getCategoriesColumnLabel();
    }

    public CommentsTableUIMap.TableRow getCommentsTableRow(int rowNumber) {
        return controlls.getContent().get(rowNumber);
    }

    public List<CommentsTableUIMap.TableRow> getAllCommentsTableRowsOnPage() {
        return controlls.getContent();
    }

    public List<Comment> getAllComments() {
        List<Comment> allComments = new ArrayList<>();
        while (true) {
            for (CommentsTableUIMap.TableRow tableRow : controlls.getContent()) {
                allComments.add(tableRow.getComment());
            }
            if (getPaginatorLinksAsStrings().contains(">")) {
                clickNextPageLink();
            } else {
                break;
            }
        }

        return allComments;
    }

    public List<Comment> getAllCommentsOnPage() {
        List<Comment> allComments = new ArrayList<>();
            for (CommentsTableUIMap.TableRow tableRow : controlls.getContent()) {
                allComments.add(tableRow.getComment());
            }
        return allComments;
    }
    //click sorting links
    public CommentsTable clickNumberSortLink() {
        controlls.getNumberSortLink().click();
        controlls = new CommentsTableUIMap();
        return this;
    }

    public CommentsTable clickCommentTextSortLink() {
        controlls.getCommentTextSortLink().click();
        controlls = new CommentsTableUIMap();
        return this;
    }

    public CommentsTable clickActiveStatusSortLink() {
        controlls.getActiveStatusSortLink().click();
        controlls = new CommentsTableUIMap();
        return this;
    }

    //paginator menu

    public String getCurrentPageNumber() {
        String paginatorText = controlls.getTableFooter().getText();
        for (Link link : controlls.getPaginationLinks()) {
            paginatorText = paginatorText.replace(link.getText(), "");
        }
        return paginatorText.trim();
    }

    public List<Link> getPaginatorLinks() {
        return controlls.getPaginationLinks();
    }

    public List<String> getPaginatorLinksAsStrings() {
        List<String> paginatorAsStrings = new ArrayList<>();
        for (Link link : getPaginatorLinks()) {
            paginatorAsStrings.add(link.getText());
        }
        return paginatorAsStrings;
    }

    public CommentsTable clickNextPageLink() {
        for (Link link : controlls.getPaginationLinks()) {
            if (link.getText().equals(">")) {
                link.click();
                break;
            }
        }

        controlls = new CommentsTableUIMap();
        return this;
    }

    public CommentsTable clickPreviousPageLink() {
        for (Link link : controlls.getPaginationLinks()) {
            if (link.getText().equals("<")) {
                link.click();
                break;
            }
        }
        controlls = new CommentsTableUIMap();
        return this;
    }

    public CommentsTable clickPageLinkByNumber(String pageNumber) {
        for (Link link : controlls.getPaginationLinks()) {
            if (link.getText().equals(pageNumber)) {
                link.click();
                break;
            }
        }
        controlls = new CommentsTableUIMap();
        return this;
    }

    public int countCommentsOnCurrentPage() {
        return controlls.getContent().size();
    }

    public int countPages() {
        int counter = 1;
        while (controlls.getTableFooter().getText().contains(">")) {
            counter++;
            clickNextPageLink();
        }
        return counter;
    }

    public CommentsTableUIMap.TableRow getCommentRowByCommentNumber(String commentNumber) {
        CommentsTableUIMap.TableRow tableRowWithCommentNumber = null;
        List<CommentsTableUIMap.TableRow> commentsTableRows = controlls.getContent();
        for (CommentsTableUIMap.TableRow tableRow : commentsTableRows) {
            if (tableRow.getComment().getNumber().equals(commentNumber)) {
                tableRowWithCommentNumber = tableRow;
                break;
            }
        }
        return tableRowWithCommentNumber;
    }

    //work with checkboxes by visible comment number
    public CommentsTable checkOffCommentByCommentNumber(String commentNumber) {
        getCommentRowByCommentNumber(commentNumber).getCheckbox().checkOff();
        return this;
    }

    public CommentsTable checkOffAllCommentsOnPage() {
        for (CommentsTableUIMap.TableRow tableRow : controlls.getContent()) {
            tableRow.getCheckbox().checkOff();
        }
        return this;
    }

    public CommentsTable uncheckCommentByCommentNumber(String commentNumber) {
        getCommentRowByCommentNumber(commentNumber).getCheckbox().unCheck();
        return this;
    }

    public CommentsTable uncheckAllCommentsOnPage() {
        for (CommentsTableUIMap.TableRow tableRow : controlls.getContent()) {
            tableRow.getCheckbox().unCheck();
        }
        return this;
    }

    public boolean isCheckedOffCommentWithNumber(String commentNumber) {
        return (getCommentRowByCommentNumber(commentNumber).getCheckbox().isCheckedOff());
    }

    //returns list of checked off comments from current page
    public List<Comment> getAllCheckedOffCommentsOnPage() {
        List<Comment> checkedOffcomments = new ArrayList<>();
        for (CommentsTableUIMap.TableRow tableRow : controlls.getContent()) {
            if (tableRow.getCheckbox().isCheckedOff()) {
                checkedOffcomments.add(tableRow.getComment());
            }
        }
        return checkedOffcomments;

    }

    //returns number of all comments
    public int countAllComments() {
        int counter = 0;
        for (int i = 1; i <= countPages(); i++) {
            counter += countCommentsOnCurrentPage();
            clickNextPageLink();
        }
        return counter;
    }
    
    public TableRow getFirstActiveCommentRow(){
        TableRow firstActiveCommentRow = null;
        for (TableRow tableRow: controlls.getContent()){
            if(tableRow.getComment().isActive()){
                firstActiveCommentRow = tableRow;
                break;
            }
        }
        return firstActiveCommentRow;    
    }
    
    public TableRow getFirstInactiveCommentRow(){
        TableRow firstActiveCommentRow = null;
        for (TableRow tableRow: controlls.getContent()){
            if(!tableRow.getComment().isActive()){
                firstActiveCommentRow = tableRow;
                break;
            }
        }
        return firstActiveCommentRow;    
    }

    /** @author Danil Zhyliaiev */
    public Comment checkOffRandomCommentOnPage() {
        int numberOfRows = getAllCommentsTableRowsOnPage().size();
        CommentsTableUIMap.TableRow tableRow = getCommentsTableRow((int) (Math.random() * numberOfRows));
        tableRow.getCheckbox().checkOff();
        return tableRow.getComment();
    }

}



