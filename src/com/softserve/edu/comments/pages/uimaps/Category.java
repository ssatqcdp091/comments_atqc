package com.softserve.edu.comments.pages.uimaps;

import com.softserve.edu.comments.tools.controls.contracts.Checkbox;
import com.softserve.edu.comments.tools.controls.contracts.Label;

/**Author S.Tsyganovskiy*/

public class Category {

    Checkbox categoryCheckbox;
    Label categoryName;

    public Category(Checkbox categoryCheckbox, Label categoryName) {
        this.categoryCheckbox = categoryCheckbox;
        this.categoryName = categoryName;
    }


    public Checkbox getCategoryCheckbox(){
       return this.categoryCheckbox;
    }

    public Label getCategoryName(){
        return this.categoryName;
    }
}
