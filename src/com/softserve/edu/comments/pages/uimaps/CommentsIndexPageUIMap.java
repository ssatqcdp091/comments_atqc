package com.softserve.edu.comments.pages.uimaps;

import com.softserve.edu.comments.pages.CommentsTable;
import com.softserve.edu.comments.pages.CommentsUIDialogConfirmPopup;
import com.softserve.edu.comments.pages.CommentsUIDialogInstructionsPopup;
import com.softserve.edu.comments.tools.WebDriverUtils;
import com.softserve.edu.comments.tools.controls.ButtonImpl;
import com.softserve.edu.comments.tools.controls.DropdownImpl;
import com.softserve.edu.comments.tools.controls.LabelImpl;
import com.softserve.edu.comments.tools.controls.LinkImpl;
import com.softserve.edu.comments.tools.controls.contracts.Button;
import com.softserve.edu.comments.tools.controls.contracts.Dropdown;
import com.softserve.edu.comments.tools.controls.contracts.Label;
import com.softserve.edu.comments.tools.controls.contracts.Link;

public class CommentsIndexPageUIMap {
    private Label logoLabel;
    private Link refreshLink;

    private Link newLink;
    private Link dublicateLink;
    private Link editLink;
    private Link deleteLink;
    private Dropdown selectAnActionDropdown;

    private Label categoryNameLabel;
    private Dropdown categoryNameDropdown;
    private Label statusLabel;
    private Dropdown statusDropdown;
    private Button applyButton;

    private Label actionResultMessage;

    /**
     * the initialization of all the visible components of the page.
     * <p>
     * the not visible components are initialized in the relevant getters (
     * delayed initialization )
     * <p>
     * the CommentsTable, the CommentsUIDialogConfirmPopup, the
     * CommentsUIDialogInstructionsPopup are not initialized in the constructor,
     * there are getters for them in the CommentsIndexPageUIMap class.
     */
    public CommentsIndexPageUIMap() {
        if (!(WebDriverUtils.getCurrentUrl().contains(".azurewebsites.net")
                && WebDriverUtils.getTitle().equals("Index"))) {
            throw new RuntimeException("This is not Index page.");
        }

        logoLabel = LabelImpl.getByCss("#title");
        refreshLink = LinkImpl.getByCss("#logindisplay>a");

        newLink = LinkImpl.getByCss(".buttonAsLink:nth-child(1)");
        dublicateLink = LinkImpl.getByCss(".buttonAsLink:nth-child(2)");
        editLink = LinkImpl.getByCss(".buttonAsLink:nth-child(3)");
        deleteLink = LinkImpl.getByCss(".buttonAsLink:nth-child(4)");
        selectAnActionDropdown = DropdownImpl.getByCss("#commandSelect");

        categoryNameLabel = LabelImpl.getByCss("#filters>form>span>label");
        categoryNameDropdown = DropdownImpl.getByCss("#SelectedCateg");
        statusLabel = LabelImpl.getByCss("#filters>form>span:nth-child(3)");
        statusDropdown = DropdownImpl.getByCss("#SelectedStatus");
        applyButton = ButtonImpl.getByCss("#applybutton");

    }

    public Label getActionResultMessage() {
        if (actionResultMessage == null) {
            actionResultMessage = LabelImpl.getById("infoField");
        }
        return actionResultMessage;
    }

    public Label getLogoLabel() {
        return logoLabel;
    }

    public Link getRefreshLink() {
        return refreshLink;
    }

    public Link getNewLink() {
        return newLink;
    }

    public Link getDublicateLink() {
        return dublicateLink;
    }

    public Link getEditLink() {
        return editLink;
    }

    public Link getDeleteLink() {
        return deleteLink;
    }

    public Dropdown getSelectAnActionDropdown() {
        return selectAnActionDropdown;
    }

    /**
     * as far as the infoField can change it's text, become hidden/visible,
     * while activities on the IndexPage provided, it should be rebuild every
     * time when it's called
     *
     * @return infoField
     */
    public Label getInfoField() {
        return LabelImpl.getByCss("#infoField");
    }

    public Label getCategoryNameLabel() {
        return categoryNameLabel;
    }

    public Dropdown getCategoryNameDropdown() {
        return categoryNameDropdown;
    }

    public Label getStatusLabel() {
        return statusLabel;
    }

    public Dropdown getStatusDropdown() {
        return statusDropdown;
    }

    public Button getApplyButton() {
        return applyButton;
    }

    /**
     * as far as the CommentsTable can be modified via IndexPage controls, the
     * new CommentsTable will be created and returned each time it is needed
     *
     * @return new CommentsTable()
     */
    public CommentsTable getCommentsTable() {
        return new CommentsTable();
    }

    /**
     * Initialises the commentsUIDialogConfirmPopup as a new object every time
     * when the method is called
     * <p>
     * the NoSuchElementException should be thrown from the ContextVisible.class
     * if there is no such element or it's components on the page.
     *
     * @return new CommentsUIDialogConfirmPopup(),
     */
    public CommentsUIDialogConfirmPopup getCommentsUIDialogConfirmPopup() {
        return new CommentsUIDialogConfirmPopup();
    }

    /**
     * Initialises the commentsUIDialogInstructionsPopup as a new object every
     * time when the method is called
     * <p>
     * the NoSuchElementException should be thrown from the ContextVisible.class
     * if there is no such element or it's components on the page.
     *
     * @return new CommentsUIDialogInstructionsPopup(),
     */
    public CommentsUIDialogInstructionsPopup getCommentsUIDialogInstructionsPopup() {
        return new CommentsUIDialogInstructionsPopup();
    }

}
