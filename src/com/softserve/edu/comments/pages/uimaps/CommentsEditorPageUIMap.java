package com.softserve.edu.comments.pages.uimaps;

import com.softserve.edu.comments.tools.WebDriverUtils;
import com.softserve.edu.comments.tools.controls.*;
import com.softserve.edu.comments.tools.controls.contracts.*;

import java.util.ArrayList;
import java.util.List;

/**Author S.Tsiganovskiy*/

public class CommentsEditorPageUIMap {

    private Link refresh;
    private Link save;
    private Link saveAndReturn;
    private Link returnLink;
    private Label incorectCommentFieldsMessage;
    private TextInput commentText;
    private TextInput commentNumber;
    private Checkbox iscommentActive;
    private List<Category> availableCategories;
    private List<Category> selectedCategories;
    private Button moveAllToSelectedCategories;
    private Button moveCheckedToSelectedCategories;
    private Button moveAllToAvailableCategories;
    private Button moveCheckedToAvailableCategories;

    public CommentsEditorPageUIMap(){
        if (!(WebDriverUtils.getCurrentUrl().toLowerCase().contains("/editor/")
                && WebDriverUtils.getTitle().equals("Editor"))) {
            throw new RuntimeException("This is not Editor page.");
        }

        this.refresh = LinkImpl.getByLinkText("Refresh");
        this.save = LinkImpl.getByXpath("//input[@value='Save']");
        this.saveAndReturn = LinkImpl.getByXpath("//input[@value='Save & Return']");
        this.returnLink = LinkImpl.getByLinkText("Return");
        this.commentText = TextInputImpl.getById("Text");
        this.commentNumber = TextInputImpl.getById("Number");
        this.iscommentActive = CheckboxImpl.getById("Active");
        this.availableCategories = new CategorySet().getAvailableCategories();
        this.selectedCategories = new CategorySet().getSelectedCategories();
        this.moveAllToSelectedCategories = ButtonImpl.getByXpath("//input[@value='>>']");
        this.moveCheckedToSelectedCategories = ButtonImpl.getByXpath("//input[@value='>']") ;
        this.moveAllToAvailableCategories = ButtonImpl.getByXpath("//input[@value='<<']");
        this.moveCheckedToAvailableCategories = ButtonImpl.getByXpath("//input[@value='<']") ;
    }
    private void set (Label msg){
        this.incorectCommentFieldsMessage = msg;
    }

    // postponed initialization of the error messages
    public Label getIncorectCommentTextMessage(){
       this.set(LabelImpl.getByCss("#errorfield"));
       return this.incorectCommentFieldsMessage;
    }

    // Getters
    public Link getRefresh(){
        return this.refresh;
    }
    public Link getSave(){
        return this.save;
    }
    public Link getSaveAndReturn(){
        return this.saveAndReturn;
    }
    public Link getReturnLink(){
        return this.returnLink;
    }
    public TextInput getCommentText(){
        return this.commentText;
    }
    public TextInput getCommentNumber(){
        return this.commentNumber;
    }
    public Checkbox getIscommentActive(){
        return this.iscommentActive;
    }
    public List<Category> getAvailableCategories(){
        return this.availableCategories;
    }
    public List<Category> getSelectedCategories(){
        return this.selectedCategories;
    }
    public Button getMoveCheckedToSelectedCategories() {
        return this.moveCheckedToSelectedCategories;
    }
    public Button getMoveAllToAvailableCategories() {
        return this.moveAllToAvailableCategories;
    }
    public Button getMoveCheckedToAvailableCategories() {
        return this.moveCheckedToAvailableCategories;
    }
    public Button getMoveAllToSelectedCategories() {
        return this.moveAllToSelectedCategories;
    }

    // Refresh CategorySet
    public void refreshCategories(){
        this.availableCategories = new CategorySet().getSelectedCategories();
        this.selectedCategories = new CategorySet().getAvailableCategories();
    }
    private class CategorySet {

        public List<Category> getAvailableCategories() {
            List<Category> availableCategories= new ArrayList<Category>();
            int numberOfCategoryRows = WebElementsList.getByCss(
                    "div#alvailablecategories > div.categoryitem").asCustomElements().size();
            for (int i = 1; i <= numberOfCategoryRows; i++) {
                availableCategories.add(new Category(CheckboxImpl.getByCss(String.format(
                        "div#alvailablecategories > div.categoryitem:nth-child(%s) > "
                                + "input[type=\"checkbox\"]", i)), LabelImpl.getByCss(String.format(
                        "div#alvailablecategories > div.categoryitem:nth-child(%s)> "
                                + "span", i))));
            }
            return availableCategories;
        }

        public List<Category> getSelectedCategories(){
            List<Category> selectedCategories = new ArrayList<Category>();
            int numberOfCategoryRows = WebElementsList.getByCss(
                    "div#selectedCategories > div.categoryitem").asCustomElements().size();
            for (int i = 1; i <= numberOfCategoryRows; i++) {
                selectedCategories.add(new Category(CheckboxImpl.getByCss(String.format(
                        "div#selectedCategories > div.categoryitem:nth-child(%s) > "
                        + "input[type=\"checkbox\"]", i)), LabelImpl.getByCss(String.format(
                        "div#selectedCategories > div.categoryitem:nth-child(%s)> "
                        + "span", i))));
            }
            return selectedCategories;
        }

    }
}



