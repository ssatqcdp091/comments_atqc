package com.softserve.edu.comments.pages.uimaps;

import com.softserve.edu.comments.models.Comment;
import com.softserve.edu.comments.tools.controls.*;
import com.softserve.edu.comments.tools.controls.contracts.Checkbox;
import com.softserve.edu.comments.tools.controls.contracts.CustomElement;
import com.softserve.edu.comments.tools.controls.contracts.Label;
import com.softserve.edu.comments.tools.controls.contracts.Link;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Danil Zhyliaiev
 */
public class CommentsTableUIMap {
    private Link numberSortLink;
    private Link commentTextSortLink;
    private Link activeStatusSortLink;
    private Label categoriesColumnLabel;
    private List<Link> paginationLinks;
    private CustomElement tableFooter;
    private ArrayList<TableRow> content;

    public CommentsTableUIMap() {
        numberSortLink = LinkImpl.getByLinkText("Number");
        commentTextSortLink = LinkImpl.getByLinkText("Comment Text");
        activeStatusSortLink = LinkImpl.getByCss("table > thead > tr > th:nth-child(3) > a");
        categoriesColumnLabel = LabelImpl.getByCss("thead > tr > th:last-child");
        paginationLinks = WebElementsList.getByCss("table > tfoot > tr > td > a").asLinks();
        tableFooter = CustomElementImpl.getByCss("table > tfoot > tr > td");
        setContent();

    }

    public Link getNumberSortLink() {
        return numberSortLink;
    }

    public Link getCommentTextSortLink() {
        return commentTextSortLink;
    }

    public Link getActiveStatusSortLink() {
        return activeStatusSortLink;
    }

    public Label getCategoriesColumnLabel() {
        return categoriesColumnLabel;
    }

    public List<Link> getPaginationLinks() {
        return paginationLinks;
    }

    public CustomElement getTableFooter() {
        return tableFooter;
    }

    public ArrayList<TableRow> getContent() {
        return content;
    }

    private void setContent() {
        content = new ArrayList<>();

        int rowsNumber = WebElementsList.getByCss("tbody > tr").asCustomElements().size();

        // read every table row and put first cell as Checkbox and other cells as Comment into the Map
        for (int i = 1; i <= rowsNumber; i++) {
            content.add(new TableRow(i));
        }
    }

    private List<Label> getTableRowCells(int rowNumber) {
        return WebElementsList.getByCss(
                String.format("tbody > tr:nth-child(%d) > td:not(:first-child)", rowNumber))
                .asLabels();
    }

    private Checkbox getChecboxByRow(int rowNumber) {
        return CheckboxImpl.getByCss(String.format("tbody > tr:nth-child(%s) > td.checkedcolumn > "
                + "input[type=\"checkbox\"]", rowNumber));
    }

    public class TableRow {

        private int rowNumber;
        private Checkbox checkbox;
        private Label commentNumber;
        private Label commentText;
        private Label activeCommentText;
        private Label categoriesString;
        private List<Label> cells;
        private Comment comment;


        public TableRow(int rowNumber) {
            this.rowNumber = rowNumber;
            checkbox = getChecboxByRow(rowNumber);
            cells = getTableRowCells(rowNumber);
            setCommentNumber();
            setCommentText();
            setActiveStatusText();
            setCategoriesString();

        }

        public Checkbox getCheckbox() {
            return checkbox;
        }

        public Comment getComment() {
            if (comment != null) {
                return comment;
            }

            String number = commentNumber.getText();
            String text = commentText.getText();
            boolean isActive = activeCommentText.getText().contains("V");
            List<String> categories = Arrays.asList(categoriesString.getText()
                    .replaceAll("\\s", "")
                    .replaceAll(";", ",")
                    .split(","));

            // return comment based on found values
            this.comment = new Comment(number, text, isActive, categories);

            return comment;
        }

        private void setCommentNumber() {
            this.commentNumber = cells.get(0);
        }

        private void setCommentText() {
            this.commentText = cells.get(1);
        }

        private void setActiveStatusText() {
            this.activeCommentText = cells.get(2);
        }

        private void setCategoriesString() {
            this.categoriesString = cells.get(3);
        }

        public Label getCommentNumber(){
            return commentNumber;
        }

        public Label getCommentText(){
            return commentText;
        }

        public Label getActiveCommentText(){
            return activeCommentText;
        }

        public Label getCategoriesString(){
            return categoriesString;
        }

    }
}
