package com.softserve.edu.comments.pages.uimaps;

import com.softserve.edu.comments.tools.controls.ButtonImpl;
import com.softserve.edu.comments.tools.controls.LabelImpl;
import com.softserve.edu.comments.tools.controls.contracts.Button;
import com.softserve.edu.comments.tools.controls.contracts.Label;

public class CommentsUIDialogConfirmPopupUIMap {

    private Label uiDialogTitle;
    private Label uiDialogMsgText;
    private Button uiDialogButtonYes;
    private Button uiDialogButtonNo;

    public CommentsUIDialogConfirmPopupUIMap() {
        uiDialogTitle = LabelImpl.getByCss("#ui-dialog-title-dialog");
        uiDialogMsgText = LabelImpl.getByCss("#msgText");
        uiDialogButtonYes = ButtonImpl.getByCss(".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only.ui-state-focus.ui-state-hover");
        uiDialogButtonNo = ButtonImpl.getByCss(".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only.ui-state-hover");
    }

    public Label getUiDialogTitle() {
        return uiDialogTitle;
    }

    public Label getUiDialogMsgText() {
        return uiDialogMsgText;
    }

    public Button getUiDialogButtonYes() {
        return uiDialogButtonYes;
    }

    public Button getUiDialogButtonNo() {
        return uiDialogButtonNo;
    }
}
