package com.softserve.edu.comments.pages.uimaps;

import com.softserve.edu.comments.tools.controls.ButtonImpl;
import com.softserve.edu.comments.tools.controls.LabelImpl;
import com.softserve.edu.comments.tools.controls.contracts.Button;
import com.softserve.edu.comments.tools.controls.contracts.Label;

public class CommentsUIDialogInstructionsPopupUIMap {

    private Label uiDialogTitle;
    private Label uiDialogMsgText;
    private Button uiDialogButtonOk;

    public CommentsUIDialogInstructionsPopupUIMap() {
        uiDialogTitle = LabelImpl.getByCss("#ui-dialog-title-dialog");
        uiDialogMsgText = LabelImpl.getByCss("#msgText");
        uiDialogButtonOk = ButtonImpl.getByXpath("//button[contains(.,'Ok')]");
    }

    public Label getUiDialogTitle() {
        return uiDialogTitle;
    }

    public Label getUiDialogMsgText() {
        return uiDialogMsgText;
    }

    public Button getUiDialogButtonOk() {
        return uiDialogButtonOk;
    }
}
