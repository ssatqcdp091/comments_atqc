package com.softserve.edu.comments.pages;

import com.softserve.edu.comments.models.Comment;
import com.softserve.edu.comments.pages.uimaps.Category;
import com.softserve.edu.comments.pages.uimaps.CommentsEditorPageUIMap;
import com.softserve.edu.comments.tools.controls.contracts.Checkbox;
import com.softserve.edu.comments.tools.controls.contracts.Label;
import com.softserve.edu.comments.tools.controls.contracts.TextInput;

import javax.swing.text.LabelView;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Karabaza Anton on 12.02.2016.
 */
public class CommentsEditorPage {
    private CommentsEditorPageUIMap controls;

    public CommentsEditorPage(){
        controls = new CommentsEditorPageUIMap();
    }

    // CommentText
    public TextInput getCommentText() {
        return controls.getCommentText();
    }

    public CommentsEditorPage setCommentText(String text){
        controls.getCommentText().clear();
        controls.getCommentText().type(text);
        return this;
    }

    // CommentNumber
    public TextInput getNumber() {
        return controls.getCommentNumber();
    }

    public CommentsEditorPage setNumber(String number){
        controls.getCommentNumber().clear();
        controls.getCommentNumber().type(number);
        return this;
    }

    // ActiveBox
    public CommentsEditorPage checkOffActiveBox(){
        controls.getIscommentActive().checkOff();
        return this;
    }

    public CommentsEditorPage uncheckActiveBox(){
        controls.getIscommentActive().unCheck();
        return this;
    }

    public Checkbox getActiveBox(){
        return controls.getIscommentActive();
    }

    //ReturnLink
    public CommentsIndexPage ReturnLinkClick(){
        controls.getReturnLink().click();
        return new CommentsIndexPage();
    }

    //Menu item links
    public CommentsEditorPage refresh(){
        controls.getRefresh().click();
        return new CommentsEditorPage();
    }

    public CommentsEditorPage save(){
        controls.getSave().click();
        return new CommentsEditorPage();
    }

    public CommentsIndexPage saveAndReturn() {
        controls.getSaveAndReturn().click();
        return new CommentsIndexPage();
    }

    //Error field
    public Label getErrorField(){
         return controls.getIncorectCommentTextMessage();
    }

    //Category actions
    public CommentsEditorPage moveAllCategoriesToSelected(){
        controls.getMoveAllToSelectedCategories().click();
        controls.refreshCategories();
        return this;
    }

    public CommentsEditorPage moveAllCategoriesToAvailable(){
        controls.getMoveAllToAvailableCategories().click();
        controls.refreshCategories();
        return this;
    }

    public CommentsEditorPage moveCheckedCategoriesToSelected(){
        controls.getMoveCheckedToSelectedCategories().click();
        controls.refreshCategories();
        return this;
    }

    public CommentsEditorPage moveCheckedCategoriesToAvailable(){
        controls.getMoveCheckedToAvailableCategories().click();
        controls.refreshCategories();
        return this;
    }

    // Check off the category in any categorybox by name
    public CommentsEditorPage checkOffCategoryByName(String categoryName){
        List<Category> availableCategories = controls.getAvailableCategories();

        for(Category category: availableCategories){
            if(category.getCategoryName().getText().equals(categoryName)){
                category.getCategoryCheckbox().checkOff();
                return this;
            }
        }

        List<Category> selectedCategories = controls.getSelectedCategories();

        for(Category category: selectedCategories){
            if(category.getCategoryName().getText().equals(categoryName)){
                category.getCategoryCheckbox().checkOff();
                return this;
            }
        }

        throw new NoSuchElementException("Such category name does not exist");
    }

    // Returns labels of categories in Available category box
    public List<Label> getAvailableCategorieLabels(){
        List<Label> availableCategories = new ArrayList<Label>();

        for (Category category: controls.getAvailableCategories()){
            availableCategories.add(category.getCategoryName());
        }

        return availableCategories;
    }

    // Returns labels of categories in Selected category box
    public List<Label> getSelectedCategorieLabels() {
        List<Label> selectedCategories = new ArrayList<Label>();

        for (Category category : controls.getSelectedCategories()) {
            selectedCategories.add(category.getCategoryName());
        }

        return selectedCategories;
    }

    // Returns checkboxes of categories in Available category box
    public List<Checkbox> getAvailableCategoriesCheckboxes() {
        List<Checkbox> availableCheckboxes = new ArrayList<>();

        for (Category category : controls.getAvailableCategories()) {
            availableCheckboxes.add(category.getCategoryCheckbox());
        }

        return availableCheckboxes;
    }

    // Returns checkboxes of categories in Selected category box
    public List<Checkbox> getSelectedCategorieCheckboxes() {
        List<Checkbox> selectedCheckboxes = new ArrayList<>();

        for (Category category : controls.getSelectedCategories()) {
            selectedCheckboxes.add(category.getCategoryCheckbox());
        }

        return selectedCheckboxes;
    }

    private List<String> getSelectedCategoriesStringList(){
        List<String> selectedCategoriesStrings = new ArrayList<String>();
        List<Label> selectedCategoriesLabels = getSelectedCategorieLabels();

        for (Label categoryLabel: selectedCategoriesLabels){
            selectedCategoriesStrings.add(categoryLabel.getText());
        }
        return selectedCategoriesStrings;
    }

    //Returns current comment from editor page
    public Comment getComment(){
       return new Comment(getNumber().getText()
                ,getCommentText().getText()
                ,getActiveBox().isCheckedOff()
                ,getSelectedCategoriesStringList());
    }

    /**@author J.Bodnar*/
    public CommentsEditorPage setAndSaveComment(Comment comment){
        setComment(comment);
        save();
        return new CommentsEditorPage();
    }

    /*@author J.Bodnar*/
    public CommentsEditorPage setComment(Comment comment) {
        setCommentText(comment.getCommentText());
        setNumber(comment.getNumber());
        if (comment.isActive()){
            checkOffActiveBox();
        }
        moveAllCategoriesToAvailable();
        for(String categoryName: comment.getCategories()){
            checkOffCategoryByName(categoryName);
        }
        moveCheckedCategoriesToSelected();

        return this;
    }
}
