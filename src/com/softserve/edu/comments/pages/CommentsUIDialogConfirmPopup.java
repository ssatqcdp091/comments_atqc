package com.softserve.edu.comments.pages;

import com.softserve.edu.comments.pages.uimaps.CommentsUIDialogConfirmPopupUIMap;
import com.softserve.edu.comments.tools.controls.contracts.Label;

public class CommentsUIDialogConfirmPopup {

    private CommentsUIDialogConfirmPopupUIMap confirmPopupUIMap;

    public CommentsUIDialogConfirmPopup() {
        this.confirmPopupUIMap = new CommentsUIDialogConfirmPopupUIMap();
    }

    public String getTitle() {
        return confirmPopupUIMap.getUiDialogTitle().getText();
    }

    public Label getMessage() {
        return confirmPopupUIMap.getUiDialogMsgText();
    }

    public void confirm() {
        confirmPopupUIMap.getUiDialogButtonYes().click();
    }

    public void decline() {
        confirmPopupUIMap.getUiDialogButtonNo().click();
    }
}
