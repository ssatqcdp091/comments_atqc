package com.softserve.edu.comments.pages;

import com.softserve.edu.comments.pages.uimaps.CommentsUIDialogInstructionsPopupUIMap;
import com.softserve.edu.comments.tools.controls.LabelImpl;
import com.softserve.edu.comments.tools.controls.contracts.Label;

public class CommentsUIDialogInstructionsPopup {
    private CommentsUIDialogInstructionsPopupUIMap instructionsPopupUIMap;

    public CommentsUIDialogInstructionsPopup() {
        this.instructionsPopupUIMap = new CommentsUIDialogInstructionsPopupUIMap();
    }

    public String getTitle() {
        return instructionsPopupUIMap.getUiDialogTitle().getText();
    }

    public Label getMessage() {
        return instructionsPopupUIMap.getUiDialogMsgText();
    }

    public void confirm() {
        instructionsPopupUIMap.getUiDialogButtonOk().click();
    }

    public void decline() {
        instructionsPopupUIMap.getUiDialogButtonOk().click();
    }
}
