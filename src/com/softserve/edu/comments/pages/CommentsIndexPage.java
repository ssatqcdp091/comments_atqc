package com.softserve.edu.comments.pages;

import com.softserve.edu.comments.models.Comment;
import com.softserve.edu.comments.pages.uimaps.CommentsIndexPageUIMap;
import com.softserve.edu.comments.tools.controls.contracts.Button;
import com.softserve.edu.comments.tools.controls.contracts.Dropdown;
import com.softserve.edu.comments.tools.controls.contracts.Label;
import com.softserve.edu.comments.tools.controls.contracts.Link;

public class CommentsIndexPage {
    private CommentsIndexPageUIMap indexPageUIMap;

    public CommentsIndexPage() {
        indexPageUIMap = new CommentsIndexPageUIMap();
    }


    // LogoLabel
    public Label getLogoLabel() {
        return indexPageUIMap.getLogoLabel();
    }


    // RefreshLink
    public Link getRefreshLink() {
        return indexPageUIMap.getRefreshLink();
    }

    public CommentsIndexPage clickRefreshLink() {
        indexPageUIMap.getRefreshLink().click();
        indexPageUIMap = new CommentsIndexPageUIMap();
        return this;
    }


    // NewLink
    public Link getNewLink() {
        return indexPageUIMap.getNewLink();
    }

    public CommentsEditorPage clickNewLink() {
        indexPageUIMap.getNewLink().click();
        return new CommentsEditorPage();
    }


    // DublicateLink
    public Link getDublicateLink() {
        return indexPageUIMap.getDublicateLink();
    }

    public CommentsEditorPage clickDublicateLinkOnCommentCheckedOff() {
            indexPageUIMap.getDublicateLink().click();
            return new CommentsEditorPage();
    }

    public CommentsUIDialogInstructionsPopup clickDublicateLinkOnNoCommentCheckedOff() {
        indexPageUIMap.getDublicateLink().click();
        return getCommentsUIDialogInstructionsPopup();
    }


    // EditLink
    public Link getEditLink() {
        return indexPageUIMap.getEditLink();
    }

    public CommentsEditorPage clickEditLinkOnCommentCheckedOff() {
            indexPageUIMap.getEditLink().click();
            return new CommentsEditorPage();
    }

    public CommentsUIDialogInstructionsPopup clickEditLinkOnNoCommentCheckedOff() {
        indexPageUIMap.getEditLink().click();
        return getCommentsUIDialogInstructionsPopup();
    }


    // DeleteLink
    private Link getDeleteLink() {
        return indexPageUIMap.getDeleteLink();
    }

    public CommentsUIDialogConfirmPopup clickDeleteLinkOnCommentCheckedOff() {
            indexPageUIMap.getDeleteLink().click();
            return getCommentsUIDialogConfirmPopup();
    }

    public Label getSuccessfulDeleteMessage(){
        return indexPageUIMap.getInfoField();
    }


    public CommentsUIDialogInstructionsPopup clickDeleteLinkOnNoCommentCheckedOff() {
        indexPageUIMap.getDeleteLink().click();
        return getCommentsUIDialogInstructionsPopup();
    }


    // SelectAnActionDropdown
    public Dropdown getSelectAnActionDropdown() {
        return indexPageUIMap.getSelectAnActionDropdown();
    }


    // CategoryNameLabel
    public Label getCategoryNameLabel() {
        return indexPageUIMap.getCategoryNameLabel();
    }


    // CategoryNameDropdown;
    public Dropdown getCategoryNameDropdown() {
        return indexPageUIMap.getCategoryNameDropdown();
    }


    // StatusLabel
    public Label getStatusLabel() {
        return indexPageUIMap.getStatusLabel();
    }


    // StatusDropdown
    public Dropdown getStatusDropdown() {
        return indexPageUIMap.getStatusDropdown();
    }


    // ApplyButton
    public Button getApplyButton() {
        return indexPageUIMap.getApplyButton();
    }


    // CommentsTable
    public CommentsTable getCommentsTable() {
        return indexPageUIMap.getCommentsTable();
    }

    // CommentsUIDialogConfirmPopup
    public CommentsUIDialogConfirmPopup getCommentsUIDialogConfirmPopup() {
        return indexPageUIMap.getCommentsUIDialogConfirmPopup();
    }

    // CommentsUIDialogInstructionsPopup
    public CommentsUIDialogInstructionsPopup getCommentsUIDialogInstructionsPopup() {
        return indexPageUIMap.getCommentsUIDialogInstructionsPopup();
    }
    /**@author J.Bodnar
     * chekes off comment with commentNumber
     * then clicks edit link
     * then build comment on Editor Page, using data from comment
     * clicks Return link*/
    public CommentsIndexPage editCommentByNumber(String commentNumber, Comment comment){
        getCommentsTable().checkOffCommentByCommentNumber(commentNumber);
        clickEditLinkOnCommentCheckedOff()
                .setAndSaveComment(comment)
                .ReturnLinkClick();
        return this;
    }
    //get infofield, is displayed after successful Activation or Inactivation of comments
    public Label getActionResultMessage(){return indexPageUIMap.getActionResultMessage();}
}