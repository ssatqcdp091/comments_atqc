package com.softserve.edu.comments.models;

import com.softserve.edu.comments.data.CategoryRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * @author J.Bodnar on 11.02.2016.
 */
public class Comment {

    private String number;
    private String commentText;
    private boolean isActive;
    private List<String> categories;


    public Comment(String number, String commentText, boolean isActive, List<String> categories) {
        this.number = number;
        this.commentText = commentText;
        this.isActive = isActive;
        if (CategoryRepository.getAvailbaleCategories().containsAll(categories)) {
            this.categories = new ArrayList<String>(categories);
        } else {
            throw new RuntimeException("Category name is not correct");
        }
    }

    public Comment(){
        this.categories = new ArrayList<String>();
    }

    public String getNumber() {
        return number;
    }

    public String getCommentText() {
        return commentText;
    }

    public boolean isActive() {
        return isActive;
    }

    public List<String> getCategories() {
        return categories;
    }

    public Comment setNumber(String number) {
        this.number = number;
        return this;
    }

    public Comment setIsActive(boolean isActive) {
        this.isActive = isActive;
        return this;
    }

    public Comment setCommentText(String commentText) {
        this.commentText = commentText;
        return this;
    }

    public Comment addCategory(String categoryName) {
        categories.add(categoryName);
        return this;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Comment comment = (Comment) o;

        if (isActive != comment.isActive) return false;
        if (number != null ? !number.equals(comment.number) : comment.number != null) return false;
        if (commentText != null ? !commentText.equals(comment.commentText) : comment.commentText != null) return false;
        return categories != null ? categories.equals(comment.categories) : comment.categories == null;

    }

    @Override
    public int hashCode() {
        int result = number != null ? number.hashCode() : 0;
        result = 31 * result + (commentText != null ? commentText.hashCode() : 0);
        result = 31 * result + (isActive ? 1 : 0);
        result = 31 * result + (categories != null ? categories.hashCode() : 0);
        return result;
    }
}

