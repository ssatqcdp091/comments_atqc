package com.softserve.edu.comments.data;

public class SpecConstantsRepository {
    public static final int MAX_NUMBER_OF_COMMENTS_ON_PAGE = 10;
    public static final String BASE_URL = "http://commentssprintone.azurewebsites.net/";
    public static final String URL_POSTFIX = "?page=";
}
