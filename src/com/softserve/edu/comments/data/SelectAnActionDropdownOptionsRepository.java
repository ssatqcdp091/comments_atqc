package com.softserve.edu.comments.data;

import java.util.Arrays;
import java.util.List;

public class SelectAnActionDropdownOptionsRepository {
    public static List<String> getOptionsList() {
        return Arrays.asList(
                "Select an Action",
                "Activate",
                "Inactivate",
                "RemoveFromCategory");
    }
}
