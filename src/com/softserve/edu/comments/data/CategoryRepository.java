package com.softserve.edu.comments.data;

import java.util.Arrays;
import java.util.List;

/**
 * @author J.Bodnar on 12.02.2016.
 */
public class CategoryRepository {

    public static List<String> getAvailbaleCategories() {
       return Arrays.asList(new String[]{"Cat0", "Cat1", "Cat2", "Cat3", "Cat4", "Cat5"});
    }
}
