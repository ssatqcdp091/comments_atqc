package com.softserve.edu.comments.tests.editor.edit;

import com.softserve.edu.comments.data.CategoryRepository;
import com.softserve.edu.comments.data.CommentRepository;
import com.softserve.edu.comments.data.SpecConstantsRepository;
import com.softserve.edu.comments.models.Comment;
import com.softserve.edu.comments.pages.CommentsEditorPage;
import com.softserve.edu.comments.pages.CommentsIndexPage;
import com.softserve.edu.comments.tools.WebDriverUtils;
import com.softserve.edu.comments.tools.controls.contracts.Checkbox;
import com.softserve.edu.comments.tools.specification.Specification;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class EditCommentPageSaveTest {
    @BeforeMethod
    public void setUp() {
        WebDriverUtils.getDriver();
        WebDriverUtils.load(SpecConstantsRepository.BASE_URL);

    }

    @AfterMethod
    public void tearDown() {
        WebDriverUtils.stop();
    }

    @Test
    public void testNewCommentPageSaveFunction() {
        CommentsIndexPage indexPage = new CommentsIndexPage();
        Comment comment = indexPage.getCommentsTable().checkOffRandomCommentOnPage();
        CommentsEditorPage editorPage = indexPage.clickEditLinkOnCommentCheckedOff();

        // Expected available categories = all categories - comment's categories.
        List<String> expectedAvailableCategories = new ArrayList<>(CategoryRepository.getAvailbaleCategories());
        expectedAvailableCategories.removeAll(comment.getCategories());

        Specification specification = Specification.get()
                .For(editorPage.getCommentText())
                    .textMatch(comment.getCommentText())            // text field = comment text
                    .next()
                .For(editorPage.getNumber())
                    .textMatch(comment.getNumber())                 // number field = comment number
                    .next()
                .For(editorPage.getActiveBox())
                    .statusMatch(comment.isActive())                // status = comment status
                    .next()
                .For(editorPage.getAvailableCategorieLabels())
                    .containsAll(expectedAvailableCategories)       // available categories = All - comments categories
                    .next()
                .For(editorPage.getSelectedCategorieLabels())
                    .containsAll(comment.getCategories())           // Selected categories =  comments categories
                    .next();

        // Check if all checkboxes in available list is unchecked by default.
        for (Checkbox checkbox : editorPage.getAvailableCategoriesCheckboxes()) {
            specification.For(checkbox).isUnChecked();
        }

        // Check if all checkboxes in selected list is unchecked by default.
        for (Checkbox checkbox : editorPage.getSelectedCategorieCheckboxes()) {
            specification.For(checkbox).isUnChecked();
        }

        comment.setIsActive(!comment.isActive());

        if (comment.isActive()) {
            editorPage.getActiveBox().unCheck();
        } else {
            editorPage.getActiveBox().checkOff();
        }

        editorPage = editorPage.save();

        specification
                .For(editorPage.getCommentText())
                    .textMatch(comment.getCommentText())            // text field = comment text
                    .next()
                .For(editorPage.getNumber())
                    .textMatch(comment.getNumber())                 // number field = comment number
                    .next()
                .For(editorPage.getActiveBox())
                    .statusMatch(!comment.isActive())                // status != comment status
                    .next()
                .For(editorPage.getAvailableCategorieLabels())
                    .containsAll(expectedAvailableCategories)       // available categories = All - comments categories
                    .next()
                .For(editorPage.getSelectedCategorieLabels())
                    .containsAll(comment.getCategories())           // Selected categories =  comments categories
                    .next();

        // Check if all checkboxes in available list is unchecked by default.
        for (Checkbox checkbox : editorPage.getAvailableCategoriesCheckboxes()) {
            specification.For(checkbox).isUnChecked();
        }

        // Check if all checkboxes in selected list is unchecked by default.
        for (Checkbox checkbox : editorPage.getSelectedCategorieCheckboxes()) {
            specification.For(checkbox).isUnChecked();
        }

        WebDriverUtils.load(SpecConstantsRepository.BASE_URL);
        List<Comment> allComments = indexPage.getCommentsTable().getAllComments();

        specification
                .For(comment)
                    .containedInList(allComments)
                    .next()
                .check();
    }
}
