package com.softserve.edu.comments.tests.editor.edit;

import com.softserve.edu.comments.data.CategoryRepository;
import com.softserve.edu.comments.data.SpecConstantsRepository;
import com.softserve.edu.comments.models.Comment;
import com.softserve.edu.comments.pages.CommentsEditorPage;
import com.softserve.edu.comments.pages.CommentsIndexPage;
import com.softserve.edu.comments.tools.WebDriverUtils;
import com.softserve.edu.comments.tools.controls.contracts.Checkbox;
import com.softserve.edu.comments.tools.specification.Specification;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class EditCommentPageDefaultValuesTest {
    @BeforeMethod
    public void setUp() {
        WebDriverUtils.getDriver();
        WebDriverUtils.load(SpecConstantsRepository.BASE_URL);
    }

    @AfterMethod
    public void tearDown() {
        WebDriverUtils.stop();
    }

    @Test
    public void testEditCommentsDefaultValues() {
        CommentsIndexPage indexPage = new CommentsIndexPage();
        Comment comment = indexPage.getCommentsTable().checkOffRandomCommentOnPage();

        // Expected available categories = all categories - comment's categories.
        List<String> expectedAvailableCategories = new ArrayList<>(CategoryRepository.getAvailbaleCategories());
        expectedAvailableCategories.removeAll(comment.getCategories());

        CommentsEditorPage editorPage = indexPage.clickEditLinkOnCommentCheckedOff();

        Specification specification = Specification.get()
                .For(editorPage.getCommentText())
                    .textMatch(comment.getCommentText())            // text field = comment text?
                    .next()
                .For(editorPage.getNumber())
                    .textMatch(comment.getNumber())                 // number field = comment number?
                    .next()
                .For(editorPage.getActiveBox())
                    .statusMatch(comment.isActive())                // active status = comment status
                    .next()
                .For(editorPage.getAvailableCategorieLabels())
                    .containsAll(expectedAvailableCategories)       // expected categories = expectedAvailableCategories
                    .next()
                .For(editorPage.getSelectedCategorieLabels())
                    .containsAll(comment.getCategories())           // selected categories = comment categories
                    .next();

        // Check if all checkboxes in available list is unchecked by default.
        for (Checkbox checkbox : editorPage.getAvailableCategoriesCheckboxes()) {
            specification.For(checkbox).isUnChecked();
        }

        // Check if all checkboxes in selected list is unchecked by default.
        for (Checkbox checkbox : editorPage.getSelectedCategorieCheckboxes()) {
            specification.For(checkbox).isUnChecked();
        }

        specification.check();
    }
}
