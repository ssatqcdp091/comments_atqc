package com.softserve.edu.comments.tests.editor.newcomment;

import com.softserve.edu.comments.data.CategoryRepository;
import com.softserve.edu.comments.data.SpecConstantsRepository;
import com.softserve.edu.comments.pages.CommentsEditorPage;
import com.softserve.edu.comments.pages.CommentsIndexPage;
import com.softserve.edu.comments.tools.WebDriverUtils;
import com.softserve.edu.comments.tools.controls.contracts.Checkbox;
import com.softserve.edu.comments.tools.specification.Specification;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class NewCommentPageDefaultValuesTest {
    @BeforeMethod
    public void setUp() {
        WebDriverUtils.getDriver();
        WebDriverUtils.load(SpecConstantsRepository.BASE_URL);
    }

    @AfterMethod
    public void tearDown() {
        WebDriverUtils.stop();
    }

    @Test
    public void testNewCommentsDefaultValues() {
        CommentsIndexPage indexPage = new CommentsIndexPage();
        CommentsEditorPage editorPage = indexPage.clickNewLink();

        Specification specification = Specification.get()
                .For(editorPage.getCommentText())
                    .textMatch("")                                              // text field must be empty
                    .next()
                .For(editorPage.getNumber())
                    .textMatch("")                                              // number field must be empty
                    .next()
                .For(editorPage.getActiveBox())
                    .isChecked()                                                // active checkbox must be checked off
                    .next()
                .For(editorPage.getAvailableCategorieLabels())
                    .containsAll(CategoryRepository.getAvailbaleCategories())   // must contain all categories
                    .next()
                .For(editorPage.getSelectedCategorieLabels())
                    .listSizeEquals(0)                                          // must be empty
                    .next();

        // Check if all checkboxes in available list is unchecked by default.
        for (Checkbox checkbox : editorPage.getAvailableCategoriesCheckboxes()) {
            specification.For(checkbox).isUnChecked();
        }

        // Check if all checkboxes in selected list is unchecked by default.
        for (Checkbox checkbox : editorPage.getSelectedCategorieCheckboxes()) {
            specification.For(checkbox).isUnChecked();
        }

        specification.check();
    }
}
