package com.softserve.edu.comments.tests.editor.newcomment;

import com.softserve.edu.comments.data.CategoryRepository;
import com.softserve.edu.comments.data.CommentRepository;
import com.softserve.edu.comments.data.SpecConstantsRepository;
import com.softserve.edu.comments.models.Comment;
import com.softserve.edu.comments.pages.CommentsEditorPage;
import com.softserve.edu.comments.pages.CommentsIndexPage;
import com.softserve.edu.comments.tools.WebDriverUtils;
import com.softserve.edu.comments.tools.controls.contracts.Checkbox;
import com.softserve.edu.comments.tools.specification.Specification;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class NewCommentPageSaveTest {
    private Comment comment = CommentRepository.getNewValidComment();
    Specification specification;

    @BeforeMethod
    public void setUp() {
        WebDriverUtils.getDriver();
        WebDriverUtils.load(SpecConstantsRepository.BASE_URL);

        List<Comment> allComments = new CommentsIndexPage().getCommentsTable().getAllComments();

        specification = Specification.get()
                .For(comment)
                    .notContainedInList(allComments)
                    .next();


        WebDriverUtils.load(SpecConstantsRepository.BASE_URL);
    }

    @AfterMethod
    public void tearDown() {
        // TODO: DB query to delete newly created comment;

        WebDriverUtils.stop();
    }

    @Test
    public void testNewCommentPageSaveFunction() {
        CommentsEditorPage editorPage = new CommentsIndexPage().clickNewLink();

        editorPage = editorPage.setAndSaveComment(comment);

        // Expected available categories = all categories - comment's categories.
        List<String> expectedAvailableCategories = new ArrayList<>(CategoryRepository.getAvailbaleCategories());
        expectedAvailableCategories.removeAll(comment.getCategories());

        Specification specification = Specification.get()
                .For(editorPage.getCommentText())
                    .textMatch("")                                          // text field is empty
                    .next()
                .For(editorPage.getNumber())
                    .textMatch("")                                          // number field is empty
                    .next()
                .For(editorPage.getActiveBox())
                    .isChecked()                                            // active status is checked off
                    .next()
                .For(editorPage.getAvailableCategorieLabels())
                    .containsAll(CategoryRepository.getAvailbaleCategories())   // available categories = all
                    .next()
                .For(editorPage.getSelectedCategorieLabels())
                    .listSizeEquals(0)                                      // none categories selected
                    .next();

        // Check if all checkboxes in available list is unchecked by default.
        for (Checkbox checkbox : editorPage.getAvailableCategoriesCheckboxes()) {
            specification.For(checkbox).isUnChecked();
        }

        // Check if all checkboxes in selected list is unchecked by default.
        for (Checkbox checkbox : editorPage.getSelectedCategorieCheckboxes()) {
            specification.For(checkbox).isUnChecked();
        }

        WebDriverUtils.load(SpecConstantsRepository.BASE_URL);
        List<Comment> allComments = new CommentsIndexPage().getCommentsTable().getAllComments();

        specification
                .For(comment)
                    .containedInList(allComments)
                    .next()
                .check();
    }
}
