package com.softserve.edu.comments.tests.editor.newcomment;

import com.softserve.edu.comments.data.SpecConstantsRepository;
import com.softserve.edu.comments.pages.CommentsEditorPage;
import com.softserve.edu.comments.pages.CommentsIndexPage;
import com.softserve.edu.comments.tools.WebDriverUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class NewCommentPageReturnTest {
    @BeforeMethod
    public void setUp() {
        WebDriverUtils.getDriver();
        WebDriverUtils.load(String.format("%s/Editor/NewComment", SpecConstantsRepository.BASE_URL));
    }

    @AfterMethod
    public void tearDown() {
        WebDriverUtils.stop();
    }

    @Test
    public void testNewCommentsPageReturn() {
        // Click on return link. if the result page isn't Index page - error will be thrown.
        CommentsIndexPage indexPage = new CommentsEditorPage().ReturnLinkClick();
    }
}
