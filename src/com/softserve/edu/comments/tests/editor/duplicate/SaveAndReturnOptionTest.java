package com.softserve.edu.comments.tests.editor.duplicate;

import com.softserve.edu.comments.data.CommentRepository;
import com.softserve.edu.comments.models.Comment;
import com.softserve.edu.comments.pages.CommentsEditorPage;
import com.softserve.edu.comments.pages.CommentsIndexPage;
import com.softserve.edu.comments.tools.WebDriverUtils;
import com.softserve.edu.comments.tools.specification.Specification;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

/**
 * Created by Karabaza Anton on 17.02.2016.
 */
public class SaveAndReturnOptionTest {
    @BeforeMethod
    public void setUp() {
        WebDriverUtils.getDriver();
        WebDriverUtils.load("http://commentssprintone.azurewebsites.net/");
    }

    @AfterMethod
    public void tearDown(){
        WebDriverUtils.stop();
    }

    @Test
    public void SaveAndReturnFunctionalityTest(){
        CommentsIndexPage indexPage = new CommentsIndexPage();
        indexPage.getCommentsTable().checkOffCommentByCommentNumber("0");
        CommentsEditorPage editorPage = indexPage.clickDublicateLinkOnCommentCheckedOff();

        Comment comment = CommentRepository.getNewValidComment();
        editorPage.setComment(comment);
        indexPage = editorPage.saveAndReturn();
        List<Comment> com = indexPage.getCommentsTable().getAllComments();

        Specification.get()
                .For(comment)
                    .containedInList(indexPage.getCommentsTable().getAllComments())
                    .next()
                .check();
    }
}
