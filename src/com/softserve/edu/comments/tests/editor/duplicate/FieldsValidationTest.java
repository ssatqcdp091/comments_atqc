package com.softserve.edu.comments.tests.editor.duplicate;

import com.softserve.edu.comments.data.CommentRepository;
import com.softserve.edu.comments.pages.CommentsEditorPage;
import com.softserve.edu.comments.pages.CommentsIndexPage;
import com.softserve.edu.comments.tools.WebDriverUtils;
import com.softserve.edu.comments.tools.specification.Specification;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Created by Karabaza1 on 17.02.2016.
 */
public class FieldsValidationTest {
    @BeforeMethod
    public void setUp() {
        WebDriverUtils.getDriver();
        WebDriverUtils.load("http://commentssprintone.azurewebsites.net/");

    }

    @AfterMethod
    public void tearDown(){
        WebDriverUtils.stop();
    }

    @Test
    public void errorMessageOnSavingWithInvalidCommentTextTest(){
        CommentsIndexPage indexPage = new CommentsIndexPage();
        indexPage.getCommentsTable().checkOffCommentByCommentNumber("0");
        CommentsEditorPage editorPage = indexPage.clickDublicateLinkOnCommentCheckedOff();

        editorPage.setAndSaveComment(CommentRepository.getInvalidSymbolsTextComment());

        Specification.get()
                .For(editorPage.getErrorField())
                    .textMatch("The Comment Text field should contain alphanumeric characters only")
                    .next()
                .check();
    }

    @Test
    public void errorMessageOnSavingWithInvalidNumberTest(){
        CommentsIndexPage indexPage = new CommentsIndexPage();
        indexPage.getCommentsTable().checkOffCommentByCommentNumber("0");
        CommentsEditorPage editorPage = indexPage.clickDublicateLinkOnCommentCheckedOff();

        editorPage.setAndSaveComment(CommentRepository.getOverBoundaryNumberComment());

        Specification.get()
                .For(editorPage.getErrorField())
                .textMatch("The Number field should contain value from 0 to 999")
                .next()
                .check();
    }

    @Test
    public void errorMessageOnSavingWithoutAnySelectedCategoryTest(){
        CommentsIndexPage indexPage = new CommentsIndexPage();
        indexPage.getCommentsTable().checkOffCommentByCommentNumber("0");
        CommentsEditorPage editorPage = indexPage.clickDublicateLinkOnCommentCheckedOff();

        editorPage.setAndSaveComment(CommentRepository.getWithoutCategoriesComment());

        Specification.get()
                .For(editorPage.getErrorField())
                .textMatch("Please, select at least one category")
                .next()
                .check();
    }

}
