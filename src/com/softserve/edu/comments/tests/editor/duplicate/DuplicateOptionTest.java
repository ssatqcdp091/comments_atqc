package com.softserve.edu.comments.tests.editor.duplicate;

import com.softserve.edu.comments.data.CategoryRepository;
import com.softserve.edu.comments.models.Comment;
import com.softserve.edu.comments.pages.CommentsEditorPage;
import com.softserve.edu.comments.pages.CommentsIndexPage;
import com.softserve.edu.comments.tools.WebDriverUtils;
import com.softserve.edu.comments.tools.specification.Specification;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Karabaza Anton on 16.02.2016.
 */
public class DuplicateOptionTest {
    @BeforeMethod
    public void setUp() {
        WebDriverUtils.getDriver();
        WebDriverUtils.load("http://commentssprintone.azurewebsites.net/");
    }

    @AfterMethod
    public void tearDown(){
        WebDriverUtils.stop();
    }

    @Test
    public void DefaultValuesTest() {
        CommentsIndexPage indexPage = new CommentsIndexPage();
        indexPage.getCommentsTable().checkOffCommentByCommentNumber("0");
        Comment comment = indexPage.getCommentsTable().getAllCheckedOffCommentsOnPage().get(0);

        List<String> availableCategories = new ArrayList<>(CategoryRepository.getAvailbaleCategories());
        availableCategories.removeAll(comment.getCategories());

        CommentsEditorPage editorPage = indexPage.clickDublicateLinkOnCommentCheckedOff();

        Specification.get()
                .For(editorPage.getCommentText())
                    .textMatch(String.format("Copy of %s", comment.getCommentText()))
                    .next()
                .For(editorPage.getNumber())
                    .textMatch("")
                    .next()
                .For(editorPage.getActiveBox())
                    .statusMatch(comment.isActive())
                    .next()
                .For(editorPage.getAvailableCategorieLabels())
                    .containsAll(availableCategories)
                    .next()
                .For(editorPage.getSelectedCategorieLabels())
                    .containsAll(comment.getCategories())
                    .next()
                .check();
    }
}
