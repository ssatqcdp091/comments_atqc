package com.softserve.edu.comments.tests.delete_function;

import com.softserve.edu.comments.pages.CommentsIndexPage;
import com.softserve.edu.comments.pages.CommentsTable;
import com.softserve.edu.comments.tools.WebDriverUtils;
import com.softserve.edu.comments.tools.specification.Specification;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**Author S.Tsiganovskiy*/


public class CheckPresenceOfDeleteConfirmationDialog {
    @BeforeMethod
    public void setUp()throws Exception {
        WebDriverUtils.getDriver();
        WebDriverUtils.load("http://comments.azurewebsites.net/");
    }

    @AfterMethod
    public void tearDown()throws Exception {
        WebDriverUtils.stop();
    }

    @Test
    public void testCheckPresenceOfDeleteConfirmationDialog() throws Exception {
        CommentsIndexPage indexPage = new CommentsIndexPage();
        CommentsTable commentsTable = new CommentsTable();
        commentsTable.checkOffAllCommentsOnPage();
        Specification.get()
                .For(indexPage.clickDeleteLinkOnCommentCheckedOff()
                .getMessage())
                .textMatch("Are you sure you want to delete the selected item(s)?")
                .next().check();
    }
}

