package com.softserve.edu.comments.tests.delete_function;

import com.softserve.edu.comments.data.SelectAnActionDropdownOptionsRepository;
import com.softserve.edu.comments.pages.CommentsIndexPage;
import com.softserve.edu.comments.pages.CommentsUIDialogInstructionsPopup;
import com.softserve.edu.comments.tools.WebDriverUtils;
import com.softserve.edu.comments.tools.specification.Specification;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**Author S.Tsiganovskiy*/

public class CheckAllertPresenceWhileDeleteNothing {
    @BeforeMethod
    public void setUp()throws Exception {
        WebDriverUtils.getDriver();
     // WebDriverUtils.load("http://commentssprintone.azurewebsites.net/");
        WebDriverUtils.load("http://comments.azurewebsites.net/");
    }

    @AfterMethod
    public void tearDown()throws Exception {
        WebDriverUtils.stop();
    }

    @Test
    public void testCheckAllertPresenceWhileDeleteNothing() throws Exception {
        CommentsIndexPage indexPage = new CommentsIndexPage();

        CommentsUIDialogInstructionsPopup dialogInstructionsPopup =
                indexPage.clickDeleteLinkOnNoCommentCheckedOff();

        Specification.get()
                .For(dialogInstructionsPopup
                .getMessage())
                .textMatch("Please select at least one comment")
                .next().check();

        dialogInstructionsPopup.confirm();

    }
}

