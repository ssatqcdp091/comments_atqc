package com.softserve.edu.comments.tests.delete_function;

import com.softserve.edu.comments.models.Comment;
import com.softserve.edu.comments.pages.CommentsIndexPage;
import com.softserve.edu.comments.pages.CommentsTable;
import com.softserve.edu.comments.pages.CommentsUIDialogConfirmPopup;
import com.softserve.edu.comments.pages.uimaps.CommentsTableUIMap;
import com.softserve.edu.comments.tools.WebDriverUtils;
import com.softserve.edu.comments.tools.specification.Specification;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

/**Author S.Tsiganovskiy*/

public class DeleteSomeComments {

    @BeforeMethod
    public void setUp()throws Exception {
        WebDriverUtils.getDriver();
        WebDriverUtils.load("http://comments.azurewebsites.net/");
    }

    @AfterMethod
    public void tearDown()throws Exception {
        WebDriverUtils.stop();
    }

    @Test
    public void testDeleteSomeComment() throws Exception {

        CommentsIndexPage indexPage = new CommentsIndexPage();
        CommentsTable commentsTableBefore = indexPage.getCommentsTable();
        List<Comment> commentListBefore = commentsTableBefore.getAllCommentsOnPage();

        String NUMBER_OF_COMMENT_TO_DELETE_1 =
                commentsTableBefore.getAllCommentsOnPage().get(5).getNumber();
        String NUMBER_OF_COMMENT_TO_DELETE_2 =
                commentsTableBefore.getAllCommentsOnPage().get(6).getNumber();

        commentsTableBefore.checkOffCommentByCommentNumber(NUMBER_OF_COMMENT_TO_DELETE_1)
                           .checkOffCommentByCommentNumber(NUMBER_OF_COMMENT_TO_DELETE_2);

        CommentsUIDialogConfirmPopup deleteConfirmationPopup =
                indexPage.clickDeleteLinkOnCommentCheckedOff();

        Specification specification = Specification.get();
        specification.For(deleteConfirmationPopup
                .getMessage())
                .textMatch("Are you sure you want to delete the selected item(s)?");


        deleteConfirmationPopup.confirm();

        specification.For(indexPage.getSuccessfulDeleteMessage())
                .textMatch("Selected comments deleted successfull")
                .next().check();

        CommentsTable commentsTableAfter = indexPage.getCommentsTable();
        List<Comment> commentListAfter = commentsTableAfter.getAllCommentsOnPage();

        boolean areCommentsDeleted = true;
            for(Comment commentAfter:commentListAfter){
                if (commentAfter.getNumber().equals(NUMBER_OF_COMMENT_TO_DELETE_1)
                        ||commentAfter.getNumber().equals(NUMBER_OF_COMMENT_TO_DELETE_2)){
                    areCommentsDeleted = false;
                    break;
                }
            }
        Assert.assertTrue(areCommentsDeleted);
    }
}
