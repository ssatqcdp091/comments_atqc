package com.softserve.edu.comments.tests.delete_function;

import com.softserve.edu.comments.models.Comment;
import com.softserve.edu.comments.pages.CommentsIndexPage;
import com.softserve.edu.comments.pages.CommentsTable;
import com.softserve.edu.comments.pages.CommentsUIDialogConfirmPopup;
import com.softserve.edu.comments.tools.WebDriverUtils;
import com.softserve.edu.comments.tools.specification.Specification;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

/**Author S.Tsiganovskiy*/
public class DeleteOneComment {
    @BeforeMethod
    public void setUp()throws Exception {
        WebDriverUtils.getDriver();
        WebDriverUtils.load("http://comments.azurewebsites.net/");
    }

    @AfterMethod
    public void tearDown()throws Exception {
        WebDriverUtils.stop();
    }

    @Test
    public void testDeleteOneComment() throws Exception {

        CommentsIndexPage indexPage = new CommentsIndexPage();
        CommentsTable commentsTableBefore = indexPage.getCommentsTable();

        String NUMBER_OF_COMMENT_TO_DELETE =
                commentsTableBefore.getAllCommentsOnPage()
                               .get(commentsTableBefore.getAllCommentsOnPage().size()-1)
                               .getNumber();



        commentsTableBefore.checkOffCommentByCommentNumber(NUMBER_OF_COMMENT_TO_DELETE);

        CommentsUIDialogConfirmPopup deleteConfirmationPopup =
                indexPage.clickDeleteLinkOnCommentCheckedOff();

        Specification specification = Specification.get();
        specification.For(deleteConfirmationPopup
                     .getMessage())
                     .textMatch("Are you sure you want to delete the selected item(s)?");


        deleteConfirmationPopup.confirm();

        specification.For(indexPage.getSuccessfulDeleteMessage())
                     .textMatch("Selected comments deleted successfull")
                     .next().check();

        CommentsTable commentsTableAfter = new CommentsTable();
        List<Comment> listOfComments =  commentsTableAfter.getAllCommentsOnPage();
        boolean isCommentDeleted = true;
        for (Comment comment:listOfComments){
            if (comment.getNumber().equals(NUMBER_OF_COMMENT_TO_DELETE)){
                isCommentDeleted = false;
                break;
            }
        }
        Assert.assertTrue(isCommentDeleted);

    }
}
