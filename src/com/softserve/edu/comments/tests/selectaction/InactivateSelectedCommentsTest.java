package com.softserve.edu.comments.tests.selectaction;


import com.softserve.edu.comments.models.Comment;
import com.softserve.edu.comments.pages.CommentsIndexPage;
import com.softserve.edu.comments.tools.WebDriverUtils;
import com.softserve.edu.comments.tools.specification.Specification;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * @author J.Bodnar on 17.02.2016.
 */

public class InactivateSelectedCommentsTest {

    @BeforeMethod
    public void setUp() {
        WebDriverUtils.getDriver();
        WebDriverUtils.load("http://commentssprintone.azurewebsites.net/");
    }

    @AfterMethod
    public void tearDown() {
        WebDriverUtils.stop();
    }


    @Test
    public void testInactivateSelectedComments() {
        //preconditions
        CommentsIndexPage indexPage = new CommentsIndexPage();
        Comment activeComment = indexPage.getCommentsTable().getFirstInactiveCommentRow().getComment();
        Comment inactiveComment = indexPage.getCommentsTable().getFirstInactiveCommentRow().getComment();
        indexPage.getCommentsTable().uncheckAllCommentsOnPage();

        //test steps
        indexPage.getCommentsTable()
                .checkOffCommentByCommentNumber(activeComment.getNumber())            //check off Active comment
                .checkOffCommentByCommentNumber(inactiveComment.getNumber());         //check off Inactive comment
        indexPage.getSelectAnActionDropdown().selectByValue("Inactivate");        //select Inactivate option
        Specification specification = Specification.get()
                .For(indexPage
                        .getCommentsTable()
                        .getCommentRowByCommentNumber(activeComment.getNumber())
                        .getActiveCommentText())                                //comment must be inactive
                .textMatch("V").next()                                           //text must be empty ""
                .For(indexPage
                        .getCommentsTable()
                        .getCommentRowByCommentNumber(inactiveComment.getNumber())    //comment must be inactive
                        .getActiveCommentText())                                //text must be empty ""
                .textMatch("V").next()
                .For(indexPage.getActionResultMessage())                       //must be on page after inactivation
                .textMatch("Comments was inactivated successfull")           //"Comments were inactivated successfully"
                .next();
        specification.check();
    }

}
