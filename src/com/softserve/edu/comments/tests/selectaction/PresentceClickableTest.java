package com.softserve.edu.comments.tests.selectaction;

import com.softserve.edu.comments.data.SelectAnActionDropdownOptionsRepository;
import com.softserve.edu.comments.data.SpecConstantsRepository;
import com.softserve.edu.comments.pages.CommentsIndexPage;
import com.softserve.edu.comments.tools.WebDriverUtils;
import com.softserve.edu.comments.tools.specification.Specification;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class PresentceClickableTest {
    @BeforeMethod
    public void setUp() throws Exception {
        WebDriverUtils.getDriver();
        WebDriverUtils.load(SpecConstantsRepository.BASE_URL);
    }

    @AfterMethod
    public void tearDown() throws Exception {
        WebDriverUtils.stop();
    }

    @Test
    public void testSelectAnOptionPresentClickable() throws Exception {
        CommentsIndexPage indexPage = new CommentsIndexPage();

        Specification.get()
                .For(indexPage.getSelectAnActionDropdown()) // is visible, is present - step 1
                .isEnabled() // the dropdown is enabled, so it can be clicked - step 2
                .optionsListMatch(
                        SelectAnActionDropdownOptionsRepository
                                .getOptionsList()) // List of all the options match - step 2
                .next()
                .check();
    }
}
