package com.softserve.edu.comments.tests.paginator;

import com.softserve.edu.comments.data.SpecConstantsRepository;
import com.softserve.edu.comments.pages.CommentsIndexPage;
import com.softserve.edu.comments.tools.WebDriverUtils;
import com.softserve.edu.comments.tools.specification.Specification;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;

public class PageLinkTest {
    @BeforeClass
    public void setUp() {
        WebDriverUtils.getDriver();
        WebDriverUtils.load(SpecConstantsRepository.BASE_URL);
    }

    @AfterClass
    public void tearDown() {
        WebDriverUtils.stop();
    }

    @Test
    public void testPageLink() throws Exception {
        CommentsIndexPage indexPage = new CommentsIndexPage();

        List<String> paginatorContent = indexPage.getCommentsTable().getPaginatorLinksAsStrings();

        paginatorContent.add(indexPage.getCommentsTable().getCurrentPageNumber()); // added to the end to be clicked on the last iteration
        Specification specification = Specification.get();
        for (String pageIndexToClick : paginatorContent) {
            if (pageIndexToClick.equals(indexPage.getCommentsTable().getCurrentPageNumber()) // exclude current, >, <.
                    || pageIndexToClick.equals("<")
                    || pageIndexToClick.equals(">")) {
                continue;
            }
            indexPage.getCommentsTable().clickPageLinkByNumber(pageIndexToClick);

            specification
                    .For(Integer.parseInt(pageIndexToClick)) // was clicked
                    .valueMatch(
                            Integer.parseInt(
                                    indexPage.getCommentsTable()
                                            .getCurrentPageNumber())) // was redirected to
                    .next();
        }
        specification.check();
    }
}
