package com.softserve.edu.comments.tests.paginator;

import com.softserve.edu.comments.data.SpecConstantsRepository;
import com.softserve.edu.comments.pages.CommentsIndexPage;
import com.softserve.edu.comments.tools.WebDriverUtils;
import com.softserve.edu.comments.tools.specification.Specification;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Paging functionality. Linking(Redirection).
 * step 2.
 * https://docs.google.com/spreadsheets/d/1CxlIl36JeogFnXGNX0B2ZMd3k_SptmrvWbInEoA5O_g/edit?usp=sharing
 */
public class MaxNumberOfCommentsOnThePageTest {
    @BeforeMethod
    public void setUp() throws Exception {
        WebDriverUtils.getDriver();
        WebDriverUtils.load(SpecConstantsRepository.BASE_URL);
    }

    @AfterMethod
    public void tearDown() throws Exception {
        WebDriverUtils.stop();
    }

    @Test
    public void testMaxNumberOfCommentsOnThePage() throws Exception {

        CommentsIndexPage indexPage = new CommentsIndexPage();

        Specification specification = Specification.get();
        //all the pages except the last one
        do {
            specification
                    .For(indexPage.getCommentsTable().countCommentsOnCurrentPage())
                    .notGreaterThan(SpecConstantsRepository.MAX_NUMBER_OF_COMMENTS_ON_PAGE)
                    .next();
            indexPage.getCommentsTable().clickNextPageLink();
        } while (indexPage.getCommentsTable().getPaginatorLinksAsStrings().contains(">"));

        // last page
        specification
                .For(indexPage.getCommentsTable().countCommentsOnCurrentPage())
                .notGreaterThan(SpecConstantsRepository.MAX_NUMBER_OF_COMMENTS_ON_PAGE)
                .next()
                .check();
    }
}
