package com.softserve.edu.comments.tests.paginator;

import com.softserve.edu.comments.data.SpecConstantsRepository;
import com.softserve.edu.comments.pages.CommentsIndexPage;
import com.softserve.edu.comments.tools.WebDriverUtils;
import com.softserve.edu.comments.tools.controls.contracts.Link;
import com.softserve.edu.comments.tools.specification.Specification;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

public class NextLinkTest {
    @BeforeMethod
    public void setUp() throws Exception {
        WebDriverUtils.getDriver();
        WebDriverUtils.load(SpecConstantsRepository.BASE_URL);
    }

    @AfterMethod
    public void tearDown() throws Exception {
        WebDriverUtils.stop();
    }

    @Test
    public void testNextLink() throws Exception {

        CommentsIndexPage indexPage = new CommentsIndexPage();
        Specification specification = Specification.get();

        do {
            List<Link> paginatorLinks = indexPage.getCommentsTable()
                    .getPaginatorLinks();

            Link nextLink = null;
            for (Link link : paginatorLinks) {
                if (link.getText().equals(">")) {
                    nextLink = link;
                }
            }

            specification
                    .For(nextLink)
                    .textMatch(">")
                    .hrefMatch(
                            String.format("%s%s%d",
                                    SpecConstantsRepository.BASE_URL,
                                    SpecConstantsRepository.URL_POSTFIX,
                                    (Integer.parseInt(
                                            indexPage.getCommentsTable().getCurrentPageNumber()) + 1)))
                    .next();
            indexPage.getCommentsTable().clickNextPageLink();
        } while (indexPage.getCommentsTable().getPaginatorLinksAsStrings().contains(">"));

        specification
                .check();

    }
}
